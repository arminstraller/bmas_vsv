#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef enum {
    WRCFGA     = 0x001,
    WRCFGB     = 0x024,
    RDCFGA     = 0x002,
    RDCFGB     = 0x026,
    RDCVA      = 0x004,
    RDCVB      = 0x006,
    RDCVC      = 0x008,
    RDCVD      = 0x00A,
    RDCVE      = 0x009,
    RDCVF      = 0x00B,
    RDAUXA     = 0x00C,
    RDAUXB     = 0x00E,
    RDAUXC     = 0x00D,
    RDAUXD     = 0x00F,
    RDSTATA    = 0x010,
    RDSTATB    = 0x012,
    WRSCTRL    = 0x014,
    WRPWM      = 0x020,
    WRPSB      = 0x01C,
    RDSCTRL    = 0x016,
    RDPWM      = 0x022,
    RSPSB      = 0x01E,
    STSCTRL    = 0x019,
    CLRSCTRL   = 0x018,
    ADCV       = 0x360,
    ADOW       = 0x348,
    CVST       = 0x327,
    ADOL       = 0x301,
    ADAX       = 0x560,
    ADAXD      = 0x500,
    AXST       = 0x527,
    ADSTAT     = 0x56A,
    ADSTATD    = 0x508,
    STATST     = 0x52F,
    ADCVAX     = 0x56F,
    ADCVAX_DCP = 0x57F,
    ADCVSC     = 0x567,
    CLRCELL    = 0x711,
    CLRAUX     = 0x712,
    CLRSTAT    = 0x713,
    PLADC      = 0x714,
    DIAGN      = 0x715,
    WRCOMM     = 0x721,
    RDCOMM     = 0x722,
    STCOMM     = 0x723
} Command_Code_t;

typedef union Command {
    struct {
        uint16_t command_code : 11;
        uint16_t address : 4;
        uint16_t is_address_command : 1;
    } command_view;
    uint8_t byte_view[2];
} Command_t;

typedef struct {
    bool           is_address_command;
    uint16_t       address;
    Command_Code_t command_code;
    bool           has_data;
    uint8_t        data[6];
} Message_t;

typedef struct {
    uint8_t  command[2];
    uint16_t command_pec;
    uint8_t  data[6];
    uint16_t data_pec;
} Packed_Message_t;

typedef struct Status_Register_Group_A {
    uint16_t SC;   //!< Sum of Cells
    uint16_t ITMP; //!< Internal Die Temperature
    uint16_t VA;   //!< Analog Power Supply Voltage
} Status_Register_Group_A_t;

//! Calculate Packet Error Code (CRC), as described in the datasheet, for a series of
//! bytes.
//!
//! \param data
//!     Pointer to the first data byte
//! \param data_length
//!     Number of bytes
//! \return
//!     Calculated PEC
uint16_t calculate_pec(uint8_t *data, uint8_t data_length);

void send_message(Message_t *message);

//! Receive data transmitted by the LTC6811 during the last transaction.
//!
//! @pre
//!     A message that requires a response by the LTC6811 was send.
//! @post
//!     The received data is checked for validity by calculating its PEC and comparing it
//!     to the transmitted counterpart.
//!
//! @param received_data
//!     Pointer to the variable where the received data should be written.
//! @return
//!     true: Message was received defect free and received data was written to
//!     received_data false: Message was erroneous -> nothing was written to received_data
bool receive_data(uint8_t received_data[6]);

//! Switch Mux to next channel
//!
//! @todo
//!     Refactoring und kontrollieren
void switch_mux(uint8_t channel);
