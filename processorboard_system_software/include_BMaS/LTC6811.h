#pragma once

#include <BMaS.h>

#include <stdbool.h>
#include <stdint.h>

typedef struct LTC6811_Config_Register {
    uint32_t ADCOPT : 1;
    uint32_t DTEN : 1; //!< Read only
    uint32_t REFON : 1;
    uint32_t GPIOx : 5;
    uint32_t VUV : 12;
    uint32_t VOV : 12;
    uint16_t DCCx : 12;
    uint16_t DCTOx : 4;
} LTC6811_Config_Register_t;

typedef enum ADC_Mode {
    _422Hz_or_1kHz  = 0x0,
    _27kHz_or_14kHz = 0x1,
    _7kHz_or_3kHz   = 0x2,
    _26Hz_or_2kHz   = 0x3
} ADC_Mode_t;

//! Initialise all modules on the bus with the configuration
//!
//! @pre
//!     You have created a configuration register set according to the LTC6811 manual.
//! @post
//!     All modules on the bus are configured according to your specification.
//!
//! @param config_register
//!     Pointer to your configuration register structure.
void LTC6811_write_config(int address, LTC6811_Config_Register_t *config_register);

typedef enum Wakeup_Length { SHORT, LONG } Wakeup_Length_t;

void LTC6811_wakeup_bus(Wakeup_Length_t wakeup_length);

//! Determine whether one of the voltage sense wires between the accumulator and the
//! LTC6811 is broken.
//!
//! @todo
//!     Describe the remaining parameters
//!
//! @param connections_with_open_wire
//!     An array where the result of the open wire detection will be written
//!     to. A specific element shows whether a voltage sence wire is broken. The first
//!     index is the accumulator stack and the second index is the respective voltage
//!     sense wire within that stack. There are 13 wires because all cells have a sense
//!     wire on their positive and negative terminal. All wires are shared between two
//!     cells except the negative wire from cell 1 and the positive wire from cell 12.
//! @return
//!     true: Open Wire detected -> evaluate connections_with_open_wire to find the
//!     specific connection
//!     false: No Open Wire detected -> evaluation of
//!     connections_with_open_wire not requirec because all elements are false
bool LTC6811_open_wire_detection(ADC_Mode_t adc_mode, bool discharge_permitted,
                                 bool connections_with_open_wire[12][13]);

//! Read current voltage measurements from all stacks.
//!
//! @pre
//!     You previously send a command that instructed the LTC6811 to measure the cell
//!     voltages and the conversion has finished.
//!
//! @param cell_voltages_100uV
//!     This array holds the measured cell voltages with the number of the stack as the
//!     first index and the number of the cell as the second index.
//!
//! @return
//!     true: cell_voltages_100uV holds valid data
//!     false: The received message was erroneous and cell_voltages_100uV does not hold
//!     valid data
bool LTC6811_read_cell_voltages(uint16_t cell_voltages_100uV[12][12]);

//! Instruct the LTC6811 to measure all cell voltages and the total stack voltage
//! (declared as sum of cells) for all stacks.
//!
//! @todo
//!     Describe the parameters
void LTC6811_measure_cell_voltages_and_sum(ADC_Mode_t adc_mode, bool discharge_permitted);

//! Read current measurements for cell voltages and total stack voltage from all stacks.
//!
//! @pre
//!     You called LTC6811_measure_cell_voltages_and_sum before.
//!
//! @param cell_voltages_100uV
//!     This array holds the measured cell voltages with the number of the stack as the
//!     first index and the number of the cell as the second index.
//! @param sum_of_cells_100uV
//!     This array holds the measured total stack voltage with the stack as first index.
//!
//! @return
//!     true: cell_voltages_100uV and sum_of_cells_100uV hold valid data
//!     false: The received message was erroneous and cell_voltages_100uV and
//!     sum_of_cells_100uV don't hold valid data.
bool LTC6811_read_cell_voltages_and_sum(uint16_t cell_voltages_100uV[12][12],
                                        uint32_t sum_of_cells_100uV[12]);

//! Instruct the LTC6811 to measure all cell voltages and the voltage of the temperature
//! sensor for a given channel.
//!
//! @todo
//!     Describe the parameters
//!
//! @param temperature_channel
//!     Select one of the 8 temperature sensors of the module.
//!     Range: 1 .. 8
void LTC6811_measure_cell_voltages_and_temperature(ADC_Mode_t adc_mode,
                                                   bool       discharge_permitted,
                                                   uint8_t    temperature_channel);

//! Read current measurements for cell voltages and the currently selected temperature
//! channel from all cells.
//!
//! @pre
//!     You called LTC6811_measure_cell_voltages_and_temperature before where a
//!     temperature channel was selected for measurement.
//!
//! @param cell_voltages_100uV
//!     This array holds the measured cell voltages with the number of the stack as the
//!     first index and the number of the cell as the second index.
//! @param temperature_sensor_voltage_100uV
//!     This array holds the measured output voltage of the selected temperature channel.
//!
//! @return
//!     true: cell_voltages_100uV and temperature_sensor_voltage_100uV hold valid data
//!     false: The received message was erroneous and cell_voltages_100uV and
//!     temperature_sensor_voltage_100uV don't hold valid data.
bool LTC6811_read_cell_voltages_and_temperature(
    uint16_t cell_voltages_100uV[12][12], uint16_t temperature_sensor_voltage_100uV[12]);
