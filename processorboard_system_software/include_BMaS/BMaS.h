//! \file
//!     Global defines and typedefs

#pragma once

#include <gpio.h>

#define LOW GPIO_PIN_RESET
#define HIGH GPIO_PIN_SET

typedef unsigned int uint;

static inline void for_delay(uint counts) {
    for (uint it = 0; it < counts; it += 1)
        ;
}
