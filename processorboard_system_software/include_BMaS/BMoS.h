#pragma once

#include <stdbool.h>

// Number of BMoS units on the bus
// The units adresses must be set in ascending order from 0 to N_BMOS - 1
#define N_BMOS 6

bool BMoS_Init(void);
void BMoS_Step(void);
