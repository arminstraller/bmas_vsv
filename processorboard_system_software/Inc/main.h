/**
  ******************************************************************************
  * File Name          : main.hpp
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define AIR_P_AUX_Pin GPIO_PIN_14
#define AIR_P_AUX_GPIO_Port GPIOC
#define HSE_IN_Pin GPIO_PIN_0
#define HSE_IN_GPIO_Port GPIOH
#define HSE_OUT_Pin GPIO_PIN_1
#define HSE_OUT_GPIO_Port GPIOH
#define LED_ERROR_Pin GPIO_PIN_0
#define LED_ERROR_GPIO_Port GPIOC
#define LED_HEARTBEAT_Pin GPIO_PIN_1
#define LED_HEARTBEAT_GPIO_Port GPIOC
#define SDC_Monitoring_Pre_Pin GPIO_PIN_3
#define SDC_Monitoring_Pre_GPIO_Port GPIOC
#define AIR_P_EN_Pin GPIO_PIN_0
#define AIR_P_EN_GPIO_Port GPIOA
#define SDC_Monitoring_TSMS_Pin GPIO_PIN_1
#define SDC_Monitoring_TSMS_GPIO_Port GPIOA
#define AIR_N_EN_Pin GPIO_PIN_2
#define AIR_N_EN_GPIO_Port GPIOA
#define SPI1_SS_n_Pin GPIO_PIN_4
#define SPI1_SS_n_GPIO_Port GPIOA
#define LED_USER1_Pin GPIO_PIN_4
#define LED_USER1_GPIO_Port GPIOC
#define LED_USER2_Pin GPIO_PIN_5
#define LED_USER2_GPIO_Port GPIOC
#define Precharge_EN_Pin GPIO_PIN_1
#define Precharge_EN_GPIO_Port GPIOB
#define SDC_Monitoring_Post_Pin GPIO_PIN_10
#define SDC_Monitoring_Post_GPIO_Port GPIOB
#define BMoS_Connected_Pin GPIO_PIN_12
#define BMoS_Connected_GPIO_Port GPIOB
#define Precharge_Active_Pin GPIO_PIN_14
#define Precharge_Active_GPIO_Port GPIOB
#define AIR_N_AUX_Pin GPIO_PIN_8
#define AIR_N_AUX_GPIO_Port GPIOC
#define SDC_MON_HV_CON_Pin GPIO_PIN_8
#define SDC_MON_HV_CON_GPIO_Port GPIOA
#define IMD_Error_Latched_Pin GPIO_PIN_10
#define IMD_Error_Latched_GPIO_Port GPIOC
#define AMS_Error_Latched_Pin GPIO_PIN_11
#define AMS_Error_Latched_GPIO_Port GPIOC
#define AMS_Error_Pin GPIO_PIN_12
#define AMS_Error_GPIO_Port GPIOC
#define CAN2_STB_Pin GPIO_PIN_4
#define CAN2_STB_GPIO_Port GPIOB
#define CAN1_STB_Pin GPIO_PIN_7
#define CAN1_STB_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
