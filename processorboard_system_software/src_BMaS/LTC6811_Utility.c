#include <BMaS.h>

#include <stdbool.h>
#include <string.h>

#include <LTC6811_Utility.h>
#include <main.h>
#include <spi.h>

Packed_Message_t output_message_buffer;
Packed_Message_t input_message_buffer;

bool tx_complete = true;

uint8_t current_mux_channel;

uint fail_cnt = 0;

uint16_t calculate_pec(uint8_t *data, uint8_t data_length) {
    // CRC polynomial for pec_calculation
    const uint16_t pec_polynomial = 0xC599;
    // Linear Feedback Register for PEC calculation with data input after last register
    // stage
    // Only the lower 15 bits of this register are used for CRC calculation
    union {
        uint16_t register_representation;
        uint8_t  byte_representation[2];
    } pec_register = {.register_representation = 0x10};

    // Current databit as stated in the datasheet
    uint8_t DIN;
    // Current feedback value as calculated by DIN xor pec_register(14)
    // This one is in turn fed back according to the crc polynomial
    uint8_t feedback_value;

    // for each data byte
    // iteration from first to last data byte
    for (uint it_data = 0; it_data < data_length; it_data += 1) {
        // for each bit in data byte
        // iteration from highest to lowest data bit
        for (int it_bit = 7; it_bit >= 0; it_bit -= 1) {
            // Get current data bit
            DIN            = (data[it_data] >> it_bit) & 0x1;
            feedback_value = DIN ^ ((pec_register.register_representation >> 14) & 0x1);
            // Perform left shift on register
            pec_register.register_representation <<= 1;
            // Apply feedback by means of pec_polynomial if feedback value is 1; do
            // nothing otherwise
            if (feedback_value >= 1)
                pec_register.register_representation ^= pec_polynomial;
        }
    }
    // Finished calculation for all data bits; finalise pec by appending a 0 as stated in
    // the LTC6811 datasheet
    pec_register.register_representation <<= 1;

    // PEC is transmitted in big endian byte order -> swap bytes
    uint8_t tmp                         = pec_register.byte_representation[0];
    pec_register.byte_representation[0] = pec_register.byte_representation[1];
    pec_register.byte_representation[1] = tmp;

    return pec_register.register_representation;
}

void send_message(Message_t *message) {
    while (!tx_complete) {
        ;
    }

    tx_complete = false;

    // Pack message
    //
    // Pack command
    union {
        struct {
            uint16_t command_code : 11;
            uint16_t address : 4;
            uint16_t is_address_command : 1;
        } command_view;
        uint8_t byte_view[2];
    } packed_command;

    packed_command.command_view.command_code = message->command_code;
    if (message->is_address_command) {
        packed_command.command_view.address = message->address;
    } else {
        packed_command.command_view.address = 0;
    }
    packed_command.command_view.is_address_command = message->is_address_command;
    // Convert byteorder to big-endian
    output_message_buffer.command[0] = packed_command.byte_view[1];
    output_message_buffer.command[1] = packed_command.byte_view[0];
    // Pack command pec
    output_message_buffer.command_pec = calculate_pec(output_message_buffer.command, 2);

    uint8_t message_length;
    // Pack data
    if (message->has_data) {
        memcpy(output_message_buffer.data, message->data, 6);
        output_message_buffer.data_pec = calculate_pec(output_message_buffer.data, 6);

        message_length = 12;
    } else {
        message_length = 4;
    }

    // @TODO
    // I exchanged asynchronous communication via DMA with the synchronous variant.
    // Thomas did it this way and it worked for him. The test revealed that synchronous
    // communication does indeed work as intended. Looks like communication via DMA leads
    // to problems. We should test everything with synchronous communication first and
    // switch to the asynchronous implementation afterwards.

    // Send message
    HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, LOW);

    // HAL_SPI_TransmitReceive_DMA(&hspi1, output_message_buffer, input_message_buffer,
    //                             message_length);
    HAL_SPI_TransmitReceive(&hspi1, (uint8_t *)&output_message_buffer,
                            (uint8_t *)&input_message_buffer, message_length, 2);

    HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, HIGH);

    tx_complete = true;
}

bool receive_data(uint8_t received_data[6]) {
    // Wait for transmission to be completed
    while (!tx_complete) {
        ;
    }

    bool data_valid;

    // Check data PEC
    uint16_t data_pec = calculate_pec(input_message_buffer.data, 6);

    if (data_pec == input_message_buffer.data_pec) {
        // Copy data if pec is valid
        data_valid = true;
        memcpy(received_data, input_message_buffer.data, 6);
    } else {
        data_valid = false;
        fail_cnt += 1;
    }

    return data_valid;
}

// @TODO
// Callback is not required for synchronous communication. Reimplement this when testing
// asynchronous communication later.

// void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi) {
//     for_delay(480);
//     HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, HIGH);

//     tx_complete = true;
// }

/*
// ToDo: Fertigstellen, Refactoring, Simon Diehl drüberschauen lassen
void LTC6811_switch_Mux(void) {
    const uint16_t write_comm = WRCOMM;
    const uint16_t start_i2c  = STCOMM;
    uint64_t       rawdata    = 0;

    Message_t message;

    message.is_address_command   = 0;
    message.address              = 0;
    message.command_code_0_to_7  = ((uint8_t *)&write_comm)[0];
    message.command_code_8_to_10 = ((uint8_t *)&write_comm)[1];
    message.has_data             = true;

    switch (current_mux_channel) {
    case 0:
        rawdata = 0x690800897000;
        break;
    case 1:
        rawdata = 0x690800997000;
        break;
    case 2:
        rawdata = 0x690800A97000;
        break;
    case 3:
        rawdata = 0x690800B97000;
        break;
    case 4:
        rawdata = 0x690800C97000;
        break;
    case 5:
        rawdata = 0x690800D97000;
        break;
    case 6:
        rawdata = 0x690800E97000;
        break;
    case 7:
        rawdata = 0x690800F97000;
        break;
    default:
        break;
    }

    message.data[0] = (uint8_t)(rawdata >> 40);
    message.data[1] = (uint8_t)(rawdata >> 32);
    message.data[2] = (uint8_t)(rawdata >> 24);
    message.data[3] = (uint8_t)(rawdata >> 16);
    message.data[4] = (uint8_t)(rawdata >> 8);
    message.data[5] = (uint8_t)rawdata;

    current_mux_channel += 1;

    if (current_mux_channel == 8) {
        current_mux_channel = 0;
    }

    send_message(&message);

    message.command_code_0_to_7  = ((uint8_t *)&start_i2c)[0];
    message.command_code_8_to_10 = ((uint8_t *)&start_i2c)[1];
    message.has_data             = false;

    send_message(&message);
}
*/
