#include <BMoS_Test.h>

#include <LTC6811_Utility.h>
#include <spi.h>
#include <string.h>

void simple_test(void) {
    static uint8_t tx_read[4]  = {0x80, 0x04, 0x77, 0xD6};
    static uint8_t tx_meas[4]  = {0x03, 0x60, 0xF4, 0x6C};
    uint8_t        tmpRxBuf[8] = {0};

    // Aufwecken
    HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, LOW);
    HAL_Delay(1);
    HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, HIGH);
    HAL_Delay(1);

    // Messung starten
    // ADCV
    HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, LOW);

    HAL_SPI_Transmit(&hspi1, tx_meas, 4, 2);

    HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, HIGH);

    for_delay(480);

    // Messen braucht 2.3 ms -> 4 ms warten
    HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, LOW);
    HAL_Delay(1);
    HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, HIGH);
    HAL_Delay(1);

    // Auslesen
    // RDCVA
    printf("Read Cell Voltage Register A\n");
    HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, LOW);

    HAL_SPI_Transmit(&hspi1, tx_read, 4, 2);
    HAL_SPI_Receive(&hspi1, tmpRxBuf, 8, 2);

    HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, HIGH);

    uint16_t pec = calculate_pec(tmpRxBuf, 6);
    union {
        uint16_t ui16;
        uint8_t  ui8a[2];
    } pec_received;

    pec_received.ui8a[0] = tmpRxBuf[6];
    pec_received.ui8a[1] = tmpRxBuf[7];

    if (pec == pec_received.ui16) {
        printf("Received Data Valid\n");
        uint16_t cell_voltages[3];

        memcpy(cell_voltages, tmpRxBuf, 6);
        printf("Cell 1 = %1.3f V\n", cell_voltages[0] / 10e3);
        printf("Cell 2 = %1.3f V\n", cell_voltages[1] / 10e3);
        printf("Cell 3 = %1.3f V\n", cell_voltages[2] / 10e3);
    } else {
        printf("Received Data Invalid: %X =/= %X\n", pec, pec_received.ui16);
    }

    for (int i = 0; i < 480; i += 1) {
        ;
    }
}
