#include <LTC6811.h>

#include <stdint.h>
#include <string.h>

#include <BMoS.h>
#include <LTC6811_Utility.h>
#include <spi.h>
#include <stm32f4xx_hal.h>

void LTC6811_write_config(int address, LTC6811_Config_Register_t *config_register) {
    Message_t message;

    message.command_code = WRCFGA;

    if (address == -1) {
        message.is_address_command = false;
    } else {
        message.is_address_command = true;
        message.address            = address;
    }

    memcpy(&message.data, config_register, 6);
    message.has_data = true;

    send_message(&message);
}

void LTC6811_wakeup_bus(Wakeup_Length_t wakeup_length) {
    switch (wakeup_length) {
    case SHORT: {
        HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, LOW);
        for_delay(250);
        HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, HIGH);
        for_delay(250);
    } break;
    case LONG: {
        HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, LOW);
        HAL_Delay(1);
        HAL_GPIO_WritePin(SPI1_SS_n_GPIO_Port, SPI1_SS_n_Pin, HIGH);
        HAL_Delay(1);
    } break;
    }
}

/*
bool LTC6811_open_wire_detection(ADC_Mode_t adc_mode, bool discharge_permitted,
                                 bool connections_with_open_wire[12][13]) {
    // The algorithm described in the LTC6811 datasheet requires two measurements to be
    // taken:
    // 1. Cell voltages after some time where current was sourced out of the LTC6811s pins
    // into the voltage measurement wires
    // 2. Cell voltages after some time where current was sinked from the voltage
    // measurement wires into the LTC6811s pins
    // Sourcing and sinking is named pull-up and pull-down.
    // Comparing these measurements yields a binary decision whether the 13 measurement
    // wires between the cells and the LTC6811 are functional or broken

    typedef struct {
        uint16_t       cell_selection : 3;
        const uint16_t one_1 : 1;
        uint16_t       discharge_permitted : 1;
        const uint16_t one_2 : 1;
        uint16_t       pull_up : 1;
        uint16_t       adc_mode : 2;
        const uint16_t one_3 : 2;
    } ADOW_Command_t;

    union {
        ADOW_Command_t command_representation;
        uint16_t       command_code_representation;
    } adow_command_code = {.command_representation.one_1 = 1,
                           .command_representation.one_2 = 1,
                           .command_representation.one_3 = 1};

    struct {
        uint16_t after_pull_up_100uV[12][12];
        uint16_t after_pull_down_100uV[12][12];
    } measured_cell_voltages;

    bool open_connection_found = false;

    adow_command_code.command_representation.cell_selection      = 0;
    adow_command_code.command_representation.discharge_permitted = discharge_permitted;
    adow_command_code.command_representation.pull_up             = true;
    adow_command_code.command_representation.adc_mode            = adc_mode;

    Message_t message;

    message.is_address_command = false;
    message.command_code       = adow_command_code.command_code_representation;
    message.has_data           = false;

    send_message(&message);
    HAL_Delay(5); // Wait for command to finish
    send_message(&message);
    HAL_Delay(5); // Wait for command to finish

    LTC6811_read_cell_voltages(measured_cell_voltages.after_pull_up_100uV);

    adow_command_code.command_representation.pull_up = false;

    message.command.command_code = adow_command_code.command_code_representation;

    send_message(&message);
    HAL_Delay(5); // Wait for command to finish
    send_message(&message);
    HAL_Delay(5); // Wait for command to finish

    LTC6811_read_cell_voltages(measured_cell_voltages.after_pull_down_100uV);

    // Determine whether a connection is open using the algorithm from the datasheet.
    // for each stack
    for (uint it_stack = 0; it_stack < 12; it_stack += 1) {
        // Corner case for C0
        if (measured_cell_voltages.after_pull_up_100uV[it_stack][0] == 0) {
            connections_with_open_wire[it_stack][0] = true;
            open_connection_found                   = true;
        } else
            connections_with_open_wire[it_stack][0] = false;

        // for each cell within the stack
        for (uint it_connection = 1; it_connection <= 11; it_connection += 1) {
            // Calculate the difference
            int cell_delta =
                measured_cell_voltages.after_pull_up_100uV[it_stack][it_connection + 1] -
                measured_cell_voltages.after_pull_down_100uV[it_stack][it_connection + 1];
            if (cell_delta < -4000) {
                connections_with_open_wire[it_stack][it_connection] = true;
                open_connection_found                               = true;
            } else
                connections_with_open_wire[it_stack][it_connection] = false;
        }

        // Corner case for C12
        if (measured_cell_voltages.after_pull_down_100uV[it_stack][11] == 0) {
            connections_with_open_wire[it_stack][12] = true;
            open_connection_found                    = true;
        } else
            connections_with_open_wire[it_stack][12] = false;
    }

    return open_connection_found;
}
*/

bool LTC6811_read_cell_voltages(uint16_t cell_voltages_100uV[12][12]) {
    // These command codes are required to read all cell voltages.
    // The codes must be used one by one
    const Command_Code_t command_codes[4] = {RDCVA, RDCVB, RDCVC, RDCVD};

    union {
        uint8_t  byte_representation[6];
        uint16_t cell_voltages_100uV_representation[3];
    } received_data;

    bool data_valid = true;

    Message_t message;

    message.is_address_command = 1;
    message.has_data           = true;

    // for each stack on the bus
    for (uint it_stack = 0; it_stack < N_BMOS; it_stack += 1) {
        message.address = it_stack;

        // for each register group send specific command
        for (uint it_command = 0; it_command < 4; it_command += 1) {
            message.command_code = command_codes[it_command];

            send_message(&message);

            bool received_data_valid = receive_data(received_data.byte_representation);

            if (received_data_valid) {
                // Interpret message
                // for each cell voltage in the message
                for (uint it_data = 0; it_data < 3; it_data += 1) {
                    cell_voltages_100uV[it_stack][3 * it_command + it_data] =
                        received_data.cell_voltages_100uV_representation[it_data];
                }
            } else {
                for (uint it = 0; it < 12; it += 1) {
                    cell_voltages_100uV[it_stack][it] = 10101;
                }
            }

            data_valid = data_valid && received_data_valid;
        }
    }

    return data_valid;
}

void LTC6811_measure_cell_voltages_and_sum(ADC_Mode_t adc_mode,
                                           bool       discharge_permitted) {
    typedef struct {
        const uint16_t seven : 4;
        uint16_t       discharge_permitted : 1;
        const uint16_t three : 2;
        uint16_t       adc_mode : 2;
        const uint16_t two : 2;
    } ADCVSC_Command_t;

    union {
        ADCVSC_Command_t command_representation;
        uint16_t         command_code_representation;
    } adcvsc_command = {.command_representation =
                            (ADCVSC_Command_t){.seven = 7, .three = 3, .two = 2}};

    adcvsc_command.command_representation.discharge_permitted = discharge_permitted;
    adcvsc_command.command_representation.adc_mode            = adc_mode;

    Message_t message;

    message.command_code       = adcvsc_command.command_code_representation;
    message.is_address_command = 0;
    message.has_data           = false;

    send_message(&message);
}

bool LTC6811_read_cell_voltages_and_sum(uint16_t cell_voltages_100uV[12][12],
                                        uint32_t sum_of_cells_100uV[12]) {
    bool data_valid = true;

    // Read cell voltages first
    bool read_data_valid = LTC6811_read_cell_voltages(cell_voltages_100uV);
    data_valid           = data_valid && read_data_valid;

    // Then read status register group A for sum of cells
    Message_t message;

    message.command_code       = RDSTATA;
    message.is_address_command = true;
    message.has_data           = true;

    // for each stack
    for (uint it_stack = 0; it_stack < N_BMOS; it_stack += 1) {
        message.address = it_stack;

        // printf("Read sum of cells\n");

        send_message(&message);

        union {
            uint8_t                   byte_representation[6];
            Status_Register_Group_A_t register_representation;
        } received_data;

        bool received_data_valid = receive_data(received_data.byte_representation);

        if (received_data_valid) {
            sum_of_cells_100uV[it_stack] = received_data.register_representation.SC * 20;
        } else {
            sum_of_cells_100uV[it_stack] = 10101;
        }

        data_valid = data_valid && received_data_valid;
    }

    return data_valid;
}

/*
void LTC6811_measure_cell_voltages_and_temperature(ADC_Mode_t adc_mode,
                                                   bool       discharge_permitted,
                                                   uint8_t    temperature_channel) {
    typedef struct {
        const uint16_t fifteen : 4;
        uint16_t       discharge_permitted : 1;
        const uint16_t three : 2;
        uint16_t       adc_mode : 2;
        const uint16_t two : 2;
    } ADCVAX_Command_t;

    // @todo
    // Uncomment when switch_mux is refactored
    // switch_mux(temperature_channel);

    union {
        ADCVAX_Command_t command_representation;
        uint16_t         command_code_representation;
    } adcvax_command = {.command_representation = {.fifteen = 15, .three = 3, .two = 2}};

    adcvax_command.command_representation.discharge_permitted = discharge_permitted;
    adcvax_command.command_representation.adc_mode            = adc_mode;

    Message_t message;

    message.command.command_code       = adcvax_command.command_code_representation;
    message.command.is_address_command = false;
    message.has_data                   = false;

    send_message(&message);
}
*/

/*
bool LTC6811_read_cell_voltages_and_temperature(
    uint16_t cell_voltages_100uV[12][12], uint16_t temperature_sensor_voltage_100uV[12]) {
    bool data_valid = true;

    // Read cell voltages first
    data_valid = data_valid && LTC6811_read_cell_voltages(cell_voltages_100uV);

    // Then read auxiliary register group A for GPIO 1 voltage
    Message_t message;

    message.command.command_code       = RDAUXA;
    message.command.is_address_command = true;
    message.has_data                   = false;

    for (uint it_stack = 0; it_stack < 12; it_stack += 1) {
        message.command.address = it_stack;

        send_message(&message);

        union {
            uint8_t  byte_representation[6];
            uint16_t GPIO_voltage_representation_100uV[3];
        } received_data;

        data_valid = data_valid && receive_data(received_data.byte_representation);

        // Get voltage from GPIO 1
        temperature_sensor_voltage_100uV[it_stack] =
            received_data.GPIO_voltage_representation_100uV[0];
    }

    // @todo remove after testing
    if (!data_valid)
        Error_Handler();

    return data_valid;
}
*/
