#include <BMoS.h>

#include <stdbool.h>
#include <string.h>

#include <LTC6811.h>
#include <gpio.h>
#include <rtwtypes.h>

struct {
    uint16_t cell_voltages_100uV[12][12];
    uint16_t cell_temperatures_100uV[12][8];
    uint32_t stack_voltages_100uV[12];
} measurements = {0};

LTC6811_Config_Register_t config_register = {
    // Configuration for 2 kHz mode
    .ADCOPT = 1,
    // Reference doesn't shut down after conversion -> faster conversion times in cyclic
    // operation
    // Wait for REFUP afterwards (max 4.4 ms)
    .REFON = 1,
    // No pull down for GPIOs
    .GPIOx = 0x1F,
    // Undervoltage detection not used therefore set to lowest possible value
    .VUV = 0,
    // Overvoltage detection not used therefore set to lowest possible value
    .VOV = 0,
    // All discharge switches enabled for checkup
    .DCCx = 0x0,
    // Discharge timer disabled
    .DCTOx = 0x0};

bool BMoS_Init(void) {
    // TODO
    // bool     connections_with_open_wire[12][13];
    // bool     open_wire_detected;

    bool init_successful = true;

    // for each device on the bus
    for (uint it_address = 0; it_address < N_BMOS; it_address += 1) {
        LTC6811_wakeup_bus(LONG);

        // for each row of discharge switches
        for (uint it_row = 0; it_row < 4; it_row += 1) {
            HAL_GPIO_TogglePin(LED_USER1_GPIO_Port, LED_USER1_Pin);

            config_register.DCCx = 0x7 << (3 * it_row);
            LTC6811_wakeup_bus(SHORT);
            LTC6811_write_config(it_address, &config_register);
            HAL_Delay(100);
        }
        // Disable discharge switches
        config_register.DCCx = 0x0;
        LTC6811_wakeup_bus(SHORT);
        LTC6811_write_config(it_address, &config_register);
				
    }

    LTC6811_wakeup_bus(LONG);
    LTC6811_write_config(-1, &config_register);

    // @TODO
    // Einmal open wire detection
    // open_wire_detected =
    //     LTC6811_open_wire_detection(_26Hz_or_2kHz, false, connections_with_open_wire);

    // @TODO
    // Handle detected open wires

    // Measure cell voltages ones to provide data for the initial Simulink step
    LTC6811_measure_cell_voltages_and_sum(_26Hz_or_2kHz, false);
    HAL_Delay(6);

    return init_successful;
}

void BMoS_Step(void) {
    // @TODO
    // daten lesen; zwischen verschiedenen modi durchwechseln
    // neu messen; zwischen verschiedenen messmodi durchwechseln
    LTC6811_wakeup_bus(SHORT);
    LTC6811_read_cell_voltages_and_sum(measurements.cell_voltages_100uV,
                                       measurements.stack_voltages_100uV);
    LTC6811_measure_cell_voltages_and_sum(_26Hz_or_2kHz, false);
}

// Simulink interface
void BMoS_Input_Outputs_wrapper(uint16_T *Cell_Voltages_100uV,
                                uint32_T *Stack_Voltages_100uV,
                                uint16_T *Cell_Temperatures_100uV) {
    memcpy(Cell_Voltages_100uV, measurements.cell_voltages_100uV, 2 * 144);
    memcpy(Stack_Voltages_100uV, measurements.stack_voltages_100uV, 4 * 12);
    memcpy(Cell_Temperatures_100uV, measurements.cell_temperatures_100uV, 2 * 96);
}

// Simulink interface
void BMoS_Input_Update_wrapper(uint16_T *Cell_Voltages_100uV,
                                uint32_T *Stack_Voltages_100uV,
                                uint16_T *Cell_Temperatures_100uV) {
}