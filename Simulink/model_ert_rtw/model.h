/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: model.h
 *
 * Code generated for Simulink model 'model'.
 *
 * Model version                  : 1.289
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Fri Jun 22 20:22:57 2018
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_model_h_
#define RTW_HEADER_model_h_
#include <string.h>
#ifndef model_COMMON_INCLUDES_
# define model_COMMON_INCLUDES_
#include <string.h>
#include "rtwtypes.h"
#include "can_message.h"
#endif                                 /* model_COMMON_INCLUDES_ */

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM RT_MODEL;

/* user code (top of export header file) */
#include "can_message.h"

/* Block signals and states (auto storage) for system '<S30>/OFF Delay' */
typedef struct {
  real_T UnitDelay_DSTATE;             /* '<S31>/Unit Delay' */
  boolean_T RelationalOperator1;       /* '<S36>/Relational Operator1' */
  boolean_T RelationalOperator1_p;     /* '<S35>/Relational Operator1' */
  boolean_T UnitDelay_DSTATE_l;        /* '<S34>/Unit Delay' */
} DW_OFFDelay;

/* Block signals and states (auto storage) for system '<S30>/ON Delay' */
typedef struct {
  real_T UnitDelay_DSTATE;             /* '<S32>/Unit Delay' */
  boolean_T RelationalOperator1;       /* '<S40>/Relational Operator1' */
  boolean_T RelationalOperator1_i;     /* '<S39>/Relational Operator1' */
  boolean_T UnitDelay_DSTATE_a;        /* '<S38>/Unit Delay' */
} DW_ONDelay;

/* Block signals and states (auto storage) for system '<Root>' */
typedef struct {
  DW_ONDelay ONDelay_l;                /* '<S69>/ON Delay' */
  DW_OFFDelay OFFDelay_e;              /* '<S69>/OFF Delay' */
  DW_ONDelay ONDelay_n;                /* '<S41>/ON Delay' */
  DW_OFFDelay OFFDelay_i;              /* '<S41>/OFF Delay' */
  DW_ONDelay ONDelay_o;                /* '<S30>/ON Delay' */
  DW_OFFDelay OFFDelay_n;              /* '<S30>/OFF Delay' */
  CAN_DATATYPE CANPack;                /* '<S20>/CAN Pack' */
  CAN_DATATYPE CANPack2;               /* '<S9>/CAN Pack2' */
  CAN_DATATYPE CANPack1;               /* '<S9>/CAN Pack1' */
  CAN_DATATYPE CANPack_k;              /* '<Root>/CAN Pack' */
  real_T CANUnpack_o1;                 /* '<S20>/CAN Unpack' */
  real_T CANUnpack_o2;                 /* '<S20>/CAN Unpack' */
  real_T CANUnpack1_o1;                /* '<S9>/CAN Unpack1' */
  real_T CANUnpack1_o2;                /* '<S9>/CAN Unpack1' */
  real_T IVT_Result_U1_mV;             /* '<S9>/CAN Unpack1' */
  real_T CANUnpack1_o4;                /* '<S9>/CAN Unpack1' */
  real_T CANUnpack1_o5;                /* '<S9>/CAN Unpack1' */
  real_T CANUnpack1_o6;                /* '<S9>/CAN Unpack1' */
  real_T CANUnpack1_o7;                /* '<S9>/CAN Unpack1' */
  real_T CANUnpack2_o1;                /* '<S9>/CAN Unpack2' */
  real_T CANUnpack2_o2;                /* '<S9>/CAN Unpack2' */
  real_T IVT_Result_U2_mV;             /* '<S9>/CAN Unpack2' */
  real_T CANUnpack2_o4;                /* '<S9>/CAN Unpack2' */
  real_T CANUnpack2_o5;                /* '<S9>/CAN Unpack2' */
  real_T CANUnpack2_o6;                /* '<S9>/CAN Unpack2' */
  real_T CANUnpack2_o7;                /* '<S9>/CAN Unpack2' */
  real_T DiscreteTransferFcn;          /* '<S55>/Discrete Transfer Fcn' */
  real_T SFunctionBuilder_DSTATE;      /* '<S3>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_b;    /* '<S1>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_bt;   /* '<S52>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_i;    /* '<S23>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_k;    /* '<S28>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_bl;   /* '<S25>/S-Function Builder' */
  real_T BMoS_Input_DSTATE;            /* '<S8>/BMoS_Input' */
  real_T SFunctionBuilder_DSTATE_h;    /* '<S7>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_j;    /* '<S12>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_l;    /* '<S6>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_c;    /* '<S18>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_kf;   /* '<S2>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_o;    /* '<S4>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_e;    /* '<S5>/S-Function Builder' */
  real_T can_tx_DSTATE;                /* '<S10>/can_tx' */
  real_T SFunctionBuilder_DSTATE_h5;   /* '<S13>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_i0;   /* '<S19>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_m;    /* '<S24>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_he;   /* '<S27>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_a;    /* '<S15>/S-Function Builder' */
  real_T DiscreteTransferFcn_states;   /* '<S55>/Discrete Transfer Fcn' */
  int32_T clockTickCounter;            /* '<Root>/Pulse Generator8' */
  int32_T clockTickCounter_g;          /* '<Root>/Pulse Generator6' */
  int_T CANUnpack_ModeSignalID;        /* '<S20>/CAN Unpack' */
  int_T CANUnpack_StatusPortID;        /* '<S20>/CAN Unpack' */
  int_T CANUnpack1_ModeSignalID;       /* '<S9>/CAN Unpack1' */
  int_T CANUnpack1_StatusPortID;       /* '<S9>/CAN Unpack1' */
  int_T CANUnpack2_ModeSignalID;       /* '<S9>/CAN Unpack2' */
  int_T CANUnpack2_StatusPortID;       /* '<S9>/CAN Unpack2' */
  int_T CANPack_ModeSignalID;          /* '<Root>/CAN Pack' */
  int_T CANUnpack_ModeSignalID_n;      /* '<Root>/CAN Unpack' */
  int_T CANUnpack_StatusPortID_c;      /* '<Root>/CAN Unpack' */
  uint32_T BMoS_Input_o2[12];          /* '<S8>/BMoS_Input' */
  uint16_T BMoS_Input_o1[144];         /* '<S8>/BMoS_Input' */
  uint16_T BMoS_Input_o3[96];          /* '<S8>/BMoS_Input' */
  uint8_T SFunctionBuilder_o2[8];      /* '<S52>/S-Function Builder' */
  uint8_T SFunctionBuilder_o2_m[8];    /* '<S28>/S-Function Builder' */
  uint8_T SFunctionBuilder_o2_l[8];    /* '<S25>/S-Function Builder' */
  uint8_T MatrixConcatenate[8];        /* '<Root>/Matrix Concatenate' */
  uint8_T SFunctionBuilder_o2_m2[8];   /* '<S27>/S-Function Builder' */
  uint8_T is_active_c1_model;          /* '<S21>/TS_Activation' */
  uint8_T is_c1_model;                 /* '<S21>/TS_Activation' */
  uint8_T is_No_AMS_Error;             /* '<S21>/TS_Activation' */
  uint8_T is_Precharge;                /* '<S21>/TS_Activation' */
  uint8_T is_Precharge_Relay_Enabled;  /* '<S21>/TS_Activation' */
  uint8_T temporalCounter_i1;          /* '<S21>/TS_Activation' */
  uint8_T temporalCounter_i2;          /* '<S21>/TS_Activation' */
  boolean_T SFunctionBuilder;          /* '<S3>/S-Function Builder' */
  boolean_T SFunctionBuilder_e;        /* '<S1>/S-Function Builder' */
  boolean_T SFunctionBuilder_o1;       /* '<S52>/S-Function Builder' */
  boolean_T SFunctionBuilder_b;        /* '<S23>/S-Function Builder' */
  boolean_T SFunctionBuilder_o1_n;     /* '<S28>/S-Function Builder' */
  boolean_T SFunctionBuilder_o1_o;     /* '<S25>/S-Function Builder' */
  boolean_T SFunctionBuilder_j;        /* '<S7>/S-Function Builder' */
  boolean_T LogicalOperator1;          /* '<S30>/Logical Operator1' */
  boolean_T LogicalOperator1_p;        /* '<S41>/Logical Operator1' */
  boolean_T SFunctionBuilder_o;        /* '<S12>/S-Function Builder' */
  boolean_T SFunctionBuilder_h;        /* '<S6>/S-Function Builder' */
  boolean_T SFunctionBuilder_f;        /* '<S18>/S-Function Builder' */
  boolean_T AIR_N_EN;                  /* '<S21>/Logical Operator' */
  boolean_T Verriegelung_des_AIR_P;    /* '<S21>/Verriegelung_des_AIR_P' */
  boolean_T PulseGenerator8;           /* '<Root>/Pulse Generator8' */
  boolean_T LogicalOperator1_o;        /* '<Root>/Logical Operator1' */
  boolean_T LogicalOperator3;          /* '<Root>/Logical Operator3' */
  boolean_T SFunctionBuilder_o1_f;     /* '<S27>/S-Function Builder' */
  boolean_T LogicalOperator5;          /* '<Root>/Logical Operator5' */
  boolean_T BMoS_Error;                /* '<S22>/Logical Operator4' */
  boolean_T LogicalOperator1_ox;       /* '<S71>/Logical Operator1' */
  boolean_T LogicalOperator3_f;        /* '<S70>/Logical Operator3' */
  boolean_T Precharge;                 /* '<S21>/TS_Activation' */
  boolean_T TS_Active;                 /* '<S21>/TS_Activation' */
  boolean_T Precharge_Finished;        /* '<S21>/TS_Activation' */
  boolean_T Precharge_Relay_EN;        /* '<S21>/TS_Activation' */
  boolean_T AMS_Error;                 /* '<S21>/TS_Activation' */
  boolean_T SDC_Opened;                /* '<S21>/TS_Activation' */
  boolean_T LogicalOperator1_i;        /* '<S43>/Logical Operator1' */
  boolean_T LogicalOperator3_a;        /* '<S42>/Logical Operator3' */
  boolean_T LogicalOperator1_n;        /* '<S32>/Logical Operator1' */
  boolean_T LogicalOperator3_c;        /* '<S31>/Logical Operator3' */
  boolean_T UnitDelay_DSTATE;          /* '<S21>/Unit Delay' */
  boolean_T VoltageEstimation_MODE;    /* '<S21>/Voltage Estimation' */
  boolean_T Subsystem_MODE;            /* '<Root>/Subsystem' */
} DW;

/* Invariant block signals (auto storage) */
typedef struct {
  const int32_T Subtract;              /* '<S2>/Subtract' */
  const int32_T Subtract_m;            /* '<S3>/Subtract' */
  const int32_T Subtract_p;            /* '<S1>/Subtract' */
  const int32_T Subtract_e;            /* '<S23>/Subtract' */
  const int32_T Subtract_a;            /* '<S7>/Subtract' */
  const int32_T Subtract_g;            /* '<S12>/Subtract' */
  const int32_T Subtract_gz;           /* '<S6>/Subtract' */
  const int32_T Subtract_pm;           /* '<S18>/Subtract' */
  const int32_T Subtract_k;            /* '<S4>/Subtract' */
  const int32_T Subtract_f;            /* '<S5>/Subtract' */
  const int32_T Subtract_l;            /* '<S13>/Subtract' */
  const int32_T Subtract_o;            /* '<S19>/Subtract' */
  const int32_T Subtract_pf;           /* '<S24>/Subtract' */
  const int32_T Subtract_ai;           /* '<S15>/Subtract' */
  const boolean_T LogicalOperator2;    /* '<S30>/Logical Operator2' */
  const boolean_T LogicalOperator2_i;  /* '<S41>/Logical Operator2' */
  const boolean_T LogicalOperator2_f;  /* '<S69>/Logical Operator2' */
} ConstB;

/* Constant parameters (auto storage) */
typedef struct {
  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S1>/Constant2'
   *   '<S2>/Constant'
   *   '<S2>/Constant2'
   *   '<S3>/Constant2'
   *   '<S4>/Constant'
   *   '<S4>/Constant1'
   *   '<S4>/Constant2'
   *   '<S5>/Constant2'
   *   '<S6>/Constant2'
   *   '<S7>/Constant2'
   *   '<S12>/Constant2'
   *   '<S13>/Constant1'
   *   '<S13>/Constant2'
   *   '<S15>/Constant2'
   *   '<S18>/Constant2'
   *   '<S19>/Constant2'
   *   '<S23>/Constant'
   *   '<S23>/Constant2'
   *   '<S24>/Constant2'
   */
  int32_T pooled8;

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S1>/Constant'
   *   '<S2>/Constant1'
   *   '<S3>/Constant'
   *   '<S5>/Constant'
   *   '<S6>/Constant'
   *   '<S12>/Constant'
   *   '<S13>/Constant'
   *   '<S15>/Constant'
   *   '<S24>/Constant'
   */
  int32_T pooled9;

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S7>/Constant'
   *   '<S15>/Constant1'
   *   '<S18>/Constant'
   *   '<S19>/Constant'
   *   '<S19>/Constant1'
   *   '<S23>/Constant1'
   */
  int32_T pooled11;

  /* Computed Parameter: Constant_Value
   * Referenced by: '<S52>/Constant'
   */
  uint32_T Constant_Value;

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S10>/Constant2'
   *   '<S25>/Constant1'
   *   '<S27>/Constant1'
   *   '<S28>/Constant1'
   *   '<S52>/Constant1'
   */
  uint32_T pooled13;

  /* Computed Parameter: Constant_Value_p
   * Referenced by: '<S28>/Constant'
   */
  uint32_T Constant_Value_p;

  /* Computed Parameter: Constant_Value_p1
   * Referenced by: '<S25>/Constant'
   */
  uint32_T Constant_Value_p1;

  /* Computed Parameter: Constant_Value_m
   * Referenced by: '<S10>/Constant'
   */
  uint32_T Constant_Value_m;

  /* Computed Parameter: Constant1_Value_l
   * Referenced by: '<S10>/Constant1'
   */
  uint32_T Constant1_Value_l;

  /* Computed Parameter: Constant_Value_c
   * Referenced by: '<S27>/Constant'
   */
  uint32_T Constant_Value_c;
} ConstP;

/* Real-time Model Data Structure */
struct tag_RTM {
  const char_T * volatile errorStatus;
};

/* Block signals and states (auto storage) */
extern DW rtDW;
extern const ConstB rtConstB;          /* constant block i/o */

/* Constant parameters (auto storage) */
extern const ConstP rtConstP;

/* Model entry point functions */
extern void model_initialize(void);

/* Customized model step function */
extern void model_step(void);

/* Real-time Model object */
extern RT_MODEL *const rtM;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S9>/CAN Pack4' : Unused code path elimination
 * Block '<S9>/CAN Unpack3' : Unused code path elimination
 * Block '<S9>/Data Type Conversion' : Unused code path elimination
 * Block '<S9>/Delay' : Unused code path elimination
 * Block '<S9>/Logical Operator' : Unused code path elimination
 * Block '<S9>/Step' : Unused code path elimination
 * Block '<Root>/Logical Operator9' : Unused code path elimination
 * Block '<S22>/Logical Operator' : Unused code path elimination
 * Block '<S22>/Maximum_Cell_Voltage_100uV' : Unused code path elimination
 * Block '<S22>/Maximum_Cell_Voltage_100uV1' : Unused code path elimination
 * Block '<S22>/MinMax1' : Unused code path elimination
 * Block '<S58>/Constant1' : Unused code path elimination
 * Block '<S58>/Data Type Conversion1' : Unused code path elimination
 * Block '<S58>/Logical Operator1' : Unused code path elimination
 * Block '<S59>/Constant2' : Unused code path elimination
 * Block '<S59>/Constant3' : Unused code path elimination
 * Block '<S62>/Data Type Conversion2' : Unused code path elimination
 * Block '<S62>/Logical Operator1' : Unused code path elimination
 * Block '<S63>/Relational Operator1' : Unused code path elimination
 * Block '<S64>/Relational Operator1' : Unused code path elimination
 * Block '<S62>/Unit Delay' : Unused code path elimination
 * Block '<S59>/Logical Operator1' : Unused code path elimination
 * Block '<S59>/Logical Operator2' : Unused code path elimination
 * Block '<S59>/Logical Operator3' : Unused code path elimination
 * Block '<S59>/Relational Operator' : Unused code path elimination
 * Block '<S59>/Sum1' : Unused code path elimination
 * Block '<S59>/Switch' : Unused code path elimination
 * Block '<S59>/Unit Delay' : Unused code path elimination
 * Block '<S60>/Constant2' : Unused code path elimination
 * Block '<S60>/Constant3' : Unused code path elimination
 * Block '<S66>/Data Type Conversion2' : Unused code path elimination
 * Block '<S66>/Logical Operator1' : Unused code path elimination
 * Block '<S67>/Relational Operator1' : Unused code path elimination
 * Block '<S68>/Relational Operator1' : Unused code path elimination
 * Block '<S66>/Unit Delay' : Unused code path elimination
 * Block '<S60>/Logical Operator1' : Unused code path elimination
 * Block '<S60>/Relational Operator' : Unused code path elimination
 * Block '<S60>/Sum1' : Unused code path elimination
 * Block '<S60>/Switch' : Unused code path elimination
 * Block '<S60>/Unit Delay' : Unused code path elimination
 * Block '<S22>/Relational Operator2' : Unused code path elimination
 * Block '<S22>/Relational Operator3' : Unused code path elimination
 * Block '<S30>/Data Type Conversion1' : Eliminate redundant data type conversion
 * Block '<S34>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S34>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S38>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S38>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S41>/Data Type Conversion1' : Eliminate redundant data type conversion
 * Block '<S45>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S45>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S49>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S49>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S62>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S66>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S69>/Data Type Conversion1' : Eliminate redundant data type conversion
 * Block '<S73>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S73>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S77>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S77>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S34>/Constant1' : Unused code path elimination
 * Block '<S34>/either edge' : Unused code path elimination
 * Block '<S34>/pos. edge' : Unused code path elimination
 * Block '<S38>/Constant1' : Unused code path elimination
 * Block '<S38>/either edge' : Unused code path elimination
 * Block '<S38>/neg. edge' : Unused code path elimination
 * Block '<S45>/Constant1' : Unused code path elimination
 * Block '<S45>/either edge' : Unused code path elimination
 * Block '<S45>/pos. edge' : Unused code path elimination
 * Block '<S49>/Constant1' : Unused code path elimination
 * Block '<S49>/either edge' : Unused code path elimination
 * Block '<S49>/neg. edge' : Unused code path elimination
 * Block '<S62>/Constant1' : Unused code path elimination
 * Block '<S62>/either edge' : Unused code path elimination
 * Block '<S62>/pos. edge' : Unused code path elimination
 * Block '<S66>/Constant1' : Unused code path elimination
 * Block '<S66>/either edge' : Unused code path elimination
 * Block '<S66>/neg. edge' : Unused code path elimination
 * Block '<S73>/Constant1' : Unused code path elimination
 * Block '<S73>/either edge' : Unused code path elimination
 * Block '<S73>/pos. edge' : Unused code path elimination
 * Block '<S77>/Constant1' : Unused code path elimination
 * Block '<S77>/either edge' : Unused code path elimination
 * Block '<S77>/neg. edge' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'model'
 * '<S1>'   : 'model/AIR_N_AUX1'
 * '<S2>'   : 'model/AIR_N_EN1'
 * '<S3>'   : 'model/AIR_P_AUX1'
 * '<S4>'   : 'model/AIR_P_EN1'
 * '<S5>'   : 'model/AMS_Error1'
 * '<S6>'   : 'model/AMS_Error_Latched1'
 * '<S7>'   : 'model/BMoS_Connected1'
 * '<S8>'   : 'model/BMoS_Input1'
 * '<S9>'   : 'model/CAN Receive Isabellenhuette1'
 * '<S10>'  : 'model/CAN Tx4'
 * '<S11>'  : 'model/Can_Transmit_Cell_Overview1'
 * '<S12>'  : 'model/IMD_Error_Latched1'
 * '<S13>'  : 'model/LED_Error1'
 * '<S14>'  : 'model/LED_Error2'
 * '<S15>'  : 'model/LED_Heartbeat1'
 * '<S16>'  : 'model/On Delay'
 * '<S17>'  : 'model/On Delay2'
 * '<S18>'  : 'model/PRCH_Active1'
 * '<S19>'  : 'model/PRCH_EN1'
 * '<S20>'  : 'model/Receive Etas1'
 * '<S21>'  : 'model/Statemachine'
 * '<S22>'  : 'model/Subsystem'
 * '<S23>'  : 'model/TSMS2'
 * '<S24>'  : 'model/USER_WHITE'
 * '<S25>'  : 'model/CAN Receive Isabellenhuette1/CAN Rx1'
 * '<S26>'  : 'model/CAN Receive Isabellenhuette1/CAN Rx2'
 * '<S27>'  : 'model/CAN Receive Isabellenhuette1/CAN Rx3'
 * '<S28>'  : 'model/CAN Receive Isabellenhuette1/CAN Rx4'
 * '<S29>'  : 'model/Can_Transmit_Cell_Overview1/CAN Tx4'
 * '<S30>'  : 'model/On Delay/Model'
 * '<S31>'  : 'model/On Delay/Model/OFF Delay'
 * '<S32>'  : 'model/On Delay/Model/ON Delay'
 * '<S33>'  : 'model/On Delay/Model/OFF Delay/Edge Detector'
 * '<S34>'  : 'model/On Delay/Model/OFF Delay/Edge Detector/Model'
 * '<S35>'  : 'model/On Delay/Model/OFF Delay/Edge Detector/Model/NEGATIVE Edge'
 * '<S36>'  : 'model/On Delay/Model/OFF Delay/Edge Detector/Model/POSITIVE Edge'
 * '<S37>'  : 'model/On Delay/Model/ON Delay/Edge Detector'
 * '<S38>'  : 'model/On Delay/Model/ON Delay/Edge Detector/Model'
 * '<S39>'  : 'model/On Delay/Model/ON Delay/Edge Detector/Model/NEGATIVE Edge'
 * '<S40>'  : 'model/On Delay/Model/ON Delay/Edge Detector/Model/POSITIVE Edge'
 * '<S41>'  : 'model/On Delay2/Model'
 * '<S42>'  : 'model/On Delay2/Model/OFF Delay'
 * '<S43>'  : 'model/On Delay2/Model/ON Delay'
 * '<S44>'  : 'model/On Delay2/Model/OFF Delay/Edge Detector'
 * '<S45>'  : 'model/On Delay2/Model/OFF Delay/Edge Detector/Model'
 * '<S46>'  : 'model/On Delay2/Model/OFF Delay/Edge Detector/Model/NEGATIVE Edge'
 * '<S47>'  : 'model/On Delay2/Model/OFF Delay/Edge Detector/Model/POSITIVE Edge'
 * '<S48>'  : 'model/On Delay2/Model/ON Delay/Edge Detector'
 * '<S49>'  : 'model/On Delay2/Model/ON Delay/Edge Detector/Model'
 * '<S50>'  : 'model/On Delay2/Model/ON Delay/Edge Detector/Model/NEGATIVE Edge'
 * '<S51>'  : 'model/On Delay2/Model/ON Delay/Edge Detector/Model/POSITIVE Edge'
 * '<S52>'  : 'model/Receive Etas1/CAN Rx4'
 * '<S53>'  : 'model/Statemachine/TS_Activation'
 * '<S54>'  : 'model/Statemachine/Voltage Estimation'
 * '<S55>'  : 'model/Statemachine/Voltage Estimation/Transfer Fcn'
 * '<S56>'  : 'model/Subsystem/On Delay1'
 * '<S57>'  : 'model/Subsystem/On Delay2'
 * '<S58>'  : 'model/Subsystem/On Delay1/Model'
 * '<S59>'  : 'model/Subsystem/On Delay1/Model/OFF Delay'
 * '<S60>'  : 'model/Subsystem/On Delay1/Model/ON Delay'
 * '<S61>'  : 'model/Subsystem/On Delay1/Model/OFF Delay/Edge Detector'
 * '<S62>'  : 'model/Subsystem/On Delay1/Model/OFF Delay/Edge Detector/Model'
 * '<S63>'  : 'model/Subsystem/On Delay1/Model/OFF Delay/Edge Detector/Model/NEGATIVE Edge'
 * '<S64>'  : 'model/Subsystem/On Delay1/Model/OFF Delay/Edge Detector/Model/POSITIVE Edge'
 * '<S65>'  : 'model/Subsystem/On Delay1/Model/ON Delay/Edge Detector'
 * '<S66>'  : 'model/Subsystem/On Delay1/Model/ON Delay/Edge Detector/Model'
 * '<S67>'  : 'model/Subsystem/On Delay1/Model/ON Delay/Edge Detector/Model/NEGATIVE Edge'
 * '<S68>'  : 'model/Subsystem/On Delay1/Model/ON Delay/Edge Detector/Model/POSITIVE Edge'
 * '<S69>'  : 'model/Subsystem/On Delay2/Model'
 * '<S70>'  : 'model/Subsystem/On Delay2/Model/OFF Delay'
 * '<S71>'  : 'model/Subsystem/On Delay2/Model/ON Delay'
 * '<S72>'  : 'model/Subsystem/On Delay2/Model/OFF Delay/Edge Detector'
 * '<S73>'  : 'model/Subsystem/On Delay2/Model/OFF Delay/Edge Detector/Model'
 * '<S74>'  : 'model/Subsystem/On Delay2/Model/OFF Delay/Edge Detector/Model/NEGATIVE Edge'
 * '<S75>'  : 'model/Subsystem/On Delay2/Model/OFF Delay/Edge Detector/Model/POSITIVE Edge'
 * '<S76>'  : 'model/Subsystem/On Delay2/Model/ON Delay/Edge Detector'
 * '<S77>'  : 'model/Subsystem/On Delay2/Model/ON Delay/Edge Detector/Model'
 * '<S78>'  : 'model/Subsystem/On Delay2/Model/ON Delay/Edge Detector/Model/NEGATIVE Edge'
 * '<S79>'  : 'model/Subsystem/On Delay2/Model/ON Delay/Edge Detector/Model/POSITIVE Edge'
 */
#endif                                 /* RTW_HEADER_model_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
