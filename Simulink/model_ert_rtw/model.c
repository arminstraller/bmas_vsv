/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: model.c
 *
 * Code generated for Simulink model 'model'.
 *
 * Model version                  : 1.289
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Fri Jun 22 20:22:57 2018
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "model.h"

/* Named constants for Chart: '<S21>/TS_Activation' */
#define IN_AMS_Error                   ((uint8_T)1U)
#define IN_AMS_Error_Active            ((uint8_T)2U)
#define IN_NO_ACTIVE_CHILD             ((uint8_T)0U)
#define IN_No_AMS_Error                ((uint8_T)3U)
#define IN_Precharge                   ((uint8_T)1U)
#define IN_Precharge_Check             ((uint8_T)1U)
#define IN_Precharge_Finished          ((uint8_T)2U)
#define IN_Precharge_Hot               ((uint8_T)3U)
#define IN_Precharge_Init              ((uint8_T)1U)
#define IN_Precharge_Relay_Enabled     ((uint8_T)2U)
#define IN_SDC_Opened                  ((uint8_T)2U)
#define IN_TS_Active                   ((uint8_T)3U)
#define IN_TS_Deactivated              ((uint8_T)4U)
#define IN_Waiting_for_Precharge       ((uint8_T)4U)

/* Block signals and states (auto storage) */
DW rtDW;

/* Real-time model */
RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;
extern void gpio_in_Outputs_wrapper(const int32_T *port_popupvalue,
  const int32_T *pin_number,
  boolean_T *gpio_in,
  const real_T *xD);
extern void gpio_in_Update_wrapper(const int32_T *port_popupvalue,
  const int32_T *pin_number,
  boolean_T *gpio_in,
  real_T *xD);
extern void can_rx_Outputs_wrapper(const uint32_T *id,
  const uint32_T *channel,
  boolean_T *received,
  uint8_T *data,
  const real_T *xD);
extern void can_rx_Update_wrapper(const uint32_T *id,
  const uint32_T *channel,
  boolean_T *received,
  uint8_T *data,
  real_T *xD);
extern void BMoS_Input_Outputs_wrapper(uint16_T *Cell_Voltages_100uV,
  uint32_T *Stack_Voltages_100uV,
  uint16_T *Cell_Temperatures_100uV,
  const real_T *xD);
extern void BMoS_Input_Update_wrapper(uint16_T *Cell_Voltages_100uV,
  uint32_T *Stack_Voltages_100uV,
  uint16_T *Cell_Temperatures_100uV,
  real_T *xD);
extern void gpio_out_Outputs_wrapper(const int32_T *port_popupvalue,
  const int32_T *pin_number,
  const boolean_T *gpio_out,
  const real_T *xD);
extern void gpio_out_Update_wrapper(const int32_T *port_popupvalue,
  const int32_T *pin_number,
  const boolean_T *gpio_out,
  real_T *xD);
extern void can_tx_Outputs_wrapper(const boolean_T *trigger,
  const uint32_T *id,
  const uint32_T *dlc,
  const uint8_T *data,
  const uint32_T *channel,
  const real_T *xD);
extern void can_tx_Update_wrapper(const boolean_T *trigger,
  const uint32_T *id,
  const uint32_T *dlc,
  const uint8_T *data,
  const uint32_T *channel,
  real_T *xD);
extern void NEGATIVEEdge(real_T rtu_Enable, boolean_T rtu_IN, boolean_T
  rtu_INprevious, boolean_T *rty_OUT);
extern void POSITIVEEdge(real_T rtu_Enable, boolean_T rtu_IN, boolean_T
  rtu_INprevious, boolean_T *rty_OUT);
extern void OFFDelay_Init(DW_OFFDelay *localDW, boolean_T rtp_ic);
extern void OFFDelay(boolean_T rtu_Enable, boolean_T rtu_IN, real_T rtu_DELAY,
                     boolean_T *rty_OUT, DW_OFFDelay *localDW, real_T rtp_Ts);
extern void ONDelay_Init(DW_ONDelay *localDW, boolean_T rtp_ic);
extern void ONDelay(boolean_T rtu_Enable, boolean_T rtu_IN, real_T rtu_DELAY,
                    boolean_T *rty_OUT, DW_ONDelay *localDW, real_T rtp_Ts);

/* Forward declaration for local functions */
static void No_AMS_Error(const boolean_T *LogicalOperator7, const real_T *Gain,
  const real_T *Gain1);

/*
 * Output and update for enable system:
 *    '<S34>/NEGATIVE Edge'
 *    '<S38>/NEGATIVE Edge'
 *    '<S45>/NEGATIVE Edge'
 *    '<S49>/NEGATIVE Edge'
 *    '<S73>/NEGATIVE Edge'
 *    '<S77>/NEGATIVE Edge'
 */
void NEGATIVEEdge(real_T rtu_Enable, boolean_T rtu_IN, boolean_T rtu_INprevious,
                  boolean_T *rty_OUT)
{
  /* Outputs for Enabled SubSystem: '<S34>/NEGATIVE Edge' incorporates:
   *  EnablePort: '<S35>/Enable'
   */
  if (rtu_Enable > 0.0) {
    /* RelationalOperator: '<S35>/Relational Operator1' */
    *rty_OUT = ((int32_T)rtu_INprevious > (int32_T)rtu_IN);
  }

  /* End of Outputs for SubSystem: '<S34>/NEGATIVE Edge' */
}

/*
 * Output and update for enable system:
 *    '<S34>/POSITIVE Edge'
 *    '<S38>/POSITIVE Edge'
 *    '<S45>/POSITIVE Edge'
 *    '<S49>/POSITIVE Edge'
 *    '<S73>/POSITIVE Edge'
 *    '<S77>/POSITIVE Edge'
 */
void POSITIVEEdge(real_T rtu_Enable, boolean_T rtu_IN, boolean_T rtu_INprevious,
                  boolean_T *rty_OUT)
{
  /* Outputs for Enabled SubSystem: '<S34>/POSITIVE Edge' incorporates:
   *  EnablePort: '<S36>/Enable'
   */
  if (rtu_Enable > 0.0) {
    /* RelationalOperator: '<S36>/Relational Operator1' */
    *rty_OUT = ((int32_T)rtu_IN > (int32_T)rtu_INprevious);
  }

  /* End of Outputs for SubSystem: '<S34>/POSITIVE Edge' */
}

/*
 * System initialize for enable system:
 *    '<S30>/OFF Delay'
 *    '<S41>/OFF Delay'
 *    '<S69>/OFF Delay'
 */
void OFFDelay_Init(DW_OFFDelay *localDW, boolean_T rtp_ic)
{
  /* InitializeConditions for UnitDelay: '<S34>/Unit Delay' */
  localDW->UnitDelay_DSTATE_l = rtp_ic;
}

/*
 * Output and update for enable system:
 *    '<S30>/OFF Delay'
 *    '<S41>/OFF Delay'
 *    '<S69>/OFF Delay'
 */
void OFFDelay(boolean_T rtu_Enable, boolean_T rtu_IN, real_T rtu_DELAY,
              boolean_T *rty_OUT, DW_OFFDelay *localDW, real_T rtp_Ts)
{
  real_T rtb_Sum1;
  boolean_T rtb_UnitDelay_n;

  /* Outputs for Enabled SubSystem: '<S30>/OFF Delay' incorporates:
   *  EnablePort: '<S31>/Enable'
   */
  if (rtu_Enable) {
    /* UnitDelay: '<S34>/Unit Delay' */
    rtb_UnitDelay_n = localDW->UnitDelay_DSTATE_l;

    /* Outputs for Enabled SubSystem: '<S34>/POSITIVE Edge' */
    POSITIVEEdge(0.0, rtu_IN, rtb_UnitDelay_n, &localDW->RelationalOperator1);

    /* End of Outputs for SubSystem: '<S34>/POSITIVE Edge' */

    /* Outputs for Enabled SubSystem: '<S34>/NEGATIVE Edge' */
    NEGATIVEEdge(1.0, rtu_IN, rtb_UnitDelay_n, &localDW->RelationalOperator1_p);

    /* End of Outputs for SubSystem: '<S34>/NEGATIVE Edge' */

    /* Switch: '<S31>/Switch' incorporates:
     *  Logic: '<S34>/Logical Operator1'
     *  UnitDelay: '<S31>/Unit Delay'
     */
    if (localDW->RelationalOperator1 || localDW->RelationalOperator1_p) {
      rtb_Sum1 = rtu_DELAY;
    } else {
      rtb_Sum1 = localDW->UnitDelay_DSTATE;
    }

    /* End of Switch: '<S31>/Switch' */

    /* Sum: '<S31>/Sum1' incorporates:
     *  Constant: '<S31>/Constant3'
     */
    rtb_Sum1 -= rtp_Ts;

    /* Logic: '<S31>/Logical Operator3' incorporates:
     *  Constant: '<S31>/Constant2'
     *  Logic: '<S31>/Logical Operator1'
     *  Logic: '<S31>/Logical Operator2'
     *  RelationalOperator: '<S31>/Relational Operator'
     */
    *rty_OUT = !((!rtu_IN) && (-rtp_Ts >= rtb_Sum1));

    /* Update for UnitDelay: '<S34>/Unit Delay' */
    localDW->UnitDelay_DSTATE_l = rtu_IN;

    /* Update for UnitDelay: '<S31>/Unit Delay' */
    localDW->UnitDelay_DSTATE = rtb_Sum1;
  }

  /* End of Outputs for SubSystem: '<S30>/OFF Delay' */
}

/*
 * System initialize for enable system:
 *    '<S30>/ON Delay'
 *    '<S41>/ON Delay'
 *    '<S69>/ON Delay'
 */
void ONDelay_Init(DW_ONDelay *localDW, boolean_T rtp_ic)
{
  /* InitializeConditions for UnitDelay: '<S38>/Unit Delay' */
  localDW->UnitDelay_DSTATE_a = rtp_ic;
}

/*
 * Output and update for enable system:
 *    '<S30>/ON Delay'
 *    '<S41>/ON Delay'
 *    '<S69>/ON Delay'
 */
void ONDelay(boolean_T rtu_Enable, boolean_T rtu_IN, real_T rtu_DELAY, boolean_T
             *rty_OUT, DW_ONDelay *localDW, real_T rtp_Ts)
{
  real_T rtb_Sum1;
  boolean_T rtb_UnitDelay_b;

  /* Outputs for Enabled SubSystem: '<S30>/ON Delay' incorporates:
   *  EnablePort: '<S32>/Enable'
   */
  if (rtu_Enable) {
    /* UnitDelay: '<S38>/Unit Delay' */
    rtb_UnitDelay_b = localDW->UnitDelay_DSTATE_a;

    /* Outputs for Enabled SubSystem: '<S38>/POSITIVE Edge' */
    POSITIVEEdge(1.0, rtu_IN, rtb_UnitDelay_b, &localDW->RelationalOperator1);

    /* End of Outputs for SubSystem: '<S38>/POSITIVE Edge' */

    /* Outputs for Enabled SubSystem: '<S38>/NEGATIVE Edge' */
    NEGATIVEEdge(0.0, rtu_IN, rtb_UnitDelay_b, &localDW->RelationalOperator1_i);

    /* End of Outputs for SubSystem: '<S38>/NEGATIVE Edge' */

    /* Switch: '<S32>/Switch' incorporates:
     *  Logic: '<S38>/Logical Operator1'
     *  UnitDelay: '<S32>/Unit Delay'
     */
    if (localDW->RelationalOperator1 || localDW->RelationalOperator1_i) {
      rtb_Sum1 = rtu_DELAY;
    } else {
      rtb_Sum1 = localDW->UnitDelay_DSTATE;
    }

    /* End of Switch: '<S32>/Switch' */

    /* Sum: '<S32>/Sum1' incorporates:
     *  Constant: '<S32>/Constant3'
     */
    rtb_Sum1 -= rtp_Ts;

    /* Logic: '<S32>/Logical Operator1' incorporates:
     *  Constant: '<S32>/Constant2'
     *  RelationalOperator: '<S32>/Relational Operator'
     */
    *rty_OUT = (rtu_IN && (-rtp_Ts >= rtb_Sum1));

    /* Update for UnitDelay: '<S38>/Unit Delay' */
    localDW->UnitDelay_DSTATE_a = rtu_IN;

    /* Update for UnitDelay: '<S32>/Unit Delay' */
    localDW->UnitDelay_DSTATE = rtb_Sum1;
  }

  /* End of Outputs for SubSystem: '<S30>/ON Delay' */
}

/* Function for Chart: '<S21>/TS_Activation' */
static void No_AMS_Error(const boolean_T *LogicalOperator7, const real_T *Gain,
  const real_T *Gain1)
{
  /* During 'No_AMS_Error': '<S53>:35' */
  switch (rtDW.is_No_AMS_Error) {
   case IN_Precharge:
    /* During 'Precharge': '<S53>:1' */
    if (!*LogicalOperator7) {
      /* Transition: '<S53>:83' */
      /* Exit Internal 'Precharge': '<S53>:1' */
      if (rtDW.is_Precharge == IN_Precharge_Relay_Enabled) {
        /* Exit Internal 'Precharge_Relay_Enabled': '<S53>:74' */
        switch (rtDW.is_Precharge_Relay_Enabled) {
         case IN_Precharge_Finished:
          /* Exit 'Precharge_Finished': '<S53>:20' */
          rtDW.Precharge_Finished = false;

          /*  Aufladung abgeschlossen */
          /*  Warten auf positives AIR */
          rtDW.is_Precharge_Relay_Enabled = IN_NO_ACTIVE_CHILD;
          break;

         case IN_Precharge_Hot:
          /* Exit 'Precharge_Hot': '<S53>:41' */
          /*  PTC hat einen erh�hten Widerstand */
          /*  Precharge dauert deutlich l�nger als erwartet */
          /*  Aufladung wird trotzdem fortgesetzt */
          rtDW.is_Precharge_Relay_Enabled = IN_NO_ACTIVE_CHILD;
          break;

         default:
          rtDW.is_Precharge_Relay_Enabled = IN_NO_ACTIVE_CHILD;
          break;
        }

        /* Exit 'Precharge_Relay_Enabled': '<S53>:74' */
        rtDW.Precharge_Relay_EN = false;

        /*  Aktivierung des Precharge Relais */
        rtDW.is_Precharge = IN_NO_ACTIVE_CHILD;
      } else {
        rtDW.is_Precharge = IN_NO_ACTIVE_CHILD;
      }

      /* Exit 'Precharge': '<S53>:1' */
      rtDW.Precharge = false;

      /*  Vorladung des Zwischenkreiskondensators */
      rtDW.is_No_AMS_Error = IN_TS_Deactivated;
      rtDW.temporalCounter_i1 = 0U;

      /* Entry 'TS_Deactivated': '<S53>:13' */
      /*  TS ausgeschaltet */
      /*  AIRs deaktiviert, Precharge deaktiviert */
    } else if (rtDW.LogicalOperator1_p) {
      /* Transition: '<S53>:86' */
      /* Transition: '<S53>:39' */
      /* Exit Internal 'Precharge': '<S53>:1' */
      if (rtDW.is_Precharge == IN_Precharge_Relay_Enabled) {
        /* Exit Internal 'Precharge_Relay_Enabled': '<S53>:74' */
        switch (rtDW.is_Precharge_Relay_Enabled) {
         case IN_Precharge_Finished:
          /* Exit 'Precharge_Finished': '<S53>:20' */
          rtDW.Precharge_Finished = false;

          /*  Aufladung abgeschlossen */
          /*  Warten auf positives AIR */
          rtDW.is_Precharge_Relay_Enabled = IN_NO_ACTIVE_CHILD;
          break;

         case IN_Precharge_Hot:
          /* Exit 'Precharge_Hot': '<S53>:41' */
          /*  PTC hat einen erh�hten Widerstand */
          /*  Precharge dauert deutlich l�nger als erwartet */
          /*  Aufladung wird trotzdem fortgesetzt */
          rtDW.is_Precharge_Relay_Enabled = IN_NO_ACTIVE_CHILD;
          break;

         default:
          rtDW.is_Precharge_Relay_Enabled = IN_NO_ACTIVE_CHILD;
          break;
        }

        /* Exit 'Precharge_Relay_Enabled': '<S53>:74' */
        rtDW.Precharge_Relay_EN = false;

        /*  Aktivierung des Precharge Relais */
        rtDW.is_Precharge = IN_NO_ACTIVE_CHILD;
      } else {
        rtDW.is_Precharge = IN_NO_ACTIVE_CHILD;
      }

      /* Exit 'Precharge': '<S53>:1' */
      rtDW.Precharge = false;

      /*  Vorladung des Zwischenkreiskondensators */
      rtDW.is_No_AMS_Error = IN_NO_ACTIVE_CHILD;
      rtDW.is_c1_model = IN_AMS_Error_Active;
    } else if (rtDW.temporalCounter_i2 >= 200U) {
      /* Transition: '<S53>:89' */
      /* Exit Internal 'Precharge': '<S53>:1' */
      if (rtDW.is_Precharge == IN_Precharge_Relay_Enabled) {
        /* Exit Internal 'Precharge_Relay_Enabled': '<S53>:74' */
        switch (rtDW.is_Precharge_Relay_Enabled) {
         case IN_Precharge_Finished:
          /* Exit 'Precharge_Finished': '<S53>:20' */
          rtDW.Precharge_Finished = false;

          /*  Aufladung abgeschlossen */
          /*  Warten auf positives AIR */
          rtDW.is_Precharge_Relay_Enabled = IN_NO_ACTIVE_CHILD;
          break;

         case IN_Precharge_Hot:
          /* Exit 'Precharge_Hot': '<S53>:41' */
          /*  PTC hat einen erh�hten Widerstand */
          /*  Precharge dauert deutlich l�nger als erwartet */
          /*  Aufladung wird trotzdem fortgesetzt */
          rtDW.is_Precharge_Relay_Enabled = IN_NO_ACTIVE_CHILD;
          break;

         default:
          rtDW.is_Precharge_Relay_Enabled = IN_NO_ACTIVE_CHILD;
          break;
        }

        /* Exit 'Precharge_Relay_Enabled': '<S53>:74' */
        rtDW.Precharge_Relay_EN = false;

        /*  Aktivierung des Precharge Relais */
        rtDW.is_Precharge = IN_NO_ACTIVE_CHILD;
      } else {
        rtDW.is_Precharge = IN_NO_ACTIVE_CHILD;
      }

      /* Exit 'Precharge': '<S53>:1' */
      rtDW.Precharge = false;

      /*  Vorladung des Zwischenkreiskondensators */
      rtDW.is_No_AMS_Error = IN_TS_Deactivated;
      rtDW.temporalCounter_i1 = 0U;

      /* Entry 'TS_Deactivated': '<S53>:13' */
      /*  TS ausgeschaltet */
      /*  AIRs deaktiviert, Precharge deaktiviert */
    } else {
      rtDW.Precharge = true;
      if (rtDW.is_Precharge == IN_Precharge_Init) {
        /* During 'Precharge_Init': '<S53>:6' */
        if (rtDW.SFunctionBuilder_e) {
          /* Transition: '<S53>:17' */
          rtDW.is_Precharge = IN_Precharge_Relay_Enabled;

          /* Entry Internal 'Precharge_Relay_Enabled': '<S53>:74' */
          /* Transition: '<S53>:75' */
          rtDW.is_Precharge_Relay_Enabled = IN_Waiting_for_Precharge;

          /* Entry 'Waiting_for_Precharge': '<S53>:16' */
          /*  AIR N wurde geschlossen */
          /*  Warten auf �berwachung */
          /*  des Precharge Relais */
        }
      } else {
        /* During 'Precharge_Relay_Enabled': '<S53>:74' */
        rtDW.Precharge_Relay_EN = true;
        switch (rtDW.is_Precharge_Relay_Enabled) {
         case IN_Precharge_Check:
          /* During 'Precharge_Check': '<S53>:18' */
          if ((rtDW.temporalCounter_i1 >= 110U) || (*Gain1 <
               rtDW.DiscreteTransferFcn * 0.9)) {
            /* Transition: '<S53>:42' */
            rtDW.is_Precharge_Relay_Enabled = IN_Precharge_Hot;
            rtDW.temporalCounter_i1 = 0U;
          } else {
            /* Transition: '<S53>:71' */
            if ((*Gain1 > *Gain * 0.9) && (rtDW.temporalCounter_i1 >= 50U)) {
              /* Transition: '<S53>:21' */
              rtDW.is_Precharge_Relay_Enabled = IN_Precharge_Finished;
            }
          }
          break;

         case IN_Precharge_Finished:
          /* During 'Precharge_Finished': '<S53>:20' */
          if (rtDW.SFunctionBuilder) {
            /* Transition: '<S53>:23' */
            /* Exit 'Precharge_Finished': '<S53>:20' */
            rtDW.Precharge_Finished = false;

            /*  Aufladung abgeschlossen */
            /*  Warten auf positives AIR */
            rtDW.is_Precharge_Relay_Enabled = IN_NO_ACTIVE_CHILD;

            /* Exit 'Precharge_Relay_Enabled': '<S53>:74' */
            rtDW.Precharge_Relay_EN = false;

            /*  Aktivierung des Precharge Relais */
            rtDW.is_Precharge = IN_NO_ACTIVE_CHILD;

            /* Exit 'Precharge': '<S53>:1' */
            rtDW.Precharge = false;

            /*  Vorladung des Zwischenkreiskondensators */
            rtDW.is_No_AMS_Error = IN_TS_Active;
          } else {
            rtDW.Precharge_Finished = true;
          }
          break;

         case IN_Precharge_Hot:
          /* During 'Precharge_Hot': '<S53>:41' */
          /* Transition: '<S53>:70' */
          if ((*Gain1 > *Gain * 0.9) && (rtDW.temporalCounter_i1 >= 50U)) {
            /* Transition: '<S53>:21' */
            /* Exit 'Precharge_Hot': '<S53>:41' */
            /*  PTC hat einen erh�hten Widerstand */
            /*  Precharge dauert deutlich l�nger als erwartet */
            /*  Aufladung wird trotzdem fortgesetzt */
            rtDW.is_Precharge_Relay_Enabled = IN_Precharge_Finished;
          }
          break;

         default:
          /* During 'Waiting_for_Precharge': '<S53>:16' */
          if (rtDW.SFunctionBuilder_f) {
            /* Transition: '<S53>:19' */
            rtDW.is_Precharge_Relay_Enabled = IN_Precharge_Check;
            rtDW.temporalCounter_i1 = 0U;

            /* Entry 'Precharge_Check': '<S53>:18' */
            /*  �berwachung des Precharge Vorgangs */
          }
          break;
        }
      }
    }
    break;

   case IN_SDC_Opened:
    /* During 'SDC_Opened': '<S53>:24' */
    if (rtDW.temporalCounter_i1 >= 12U) {
      /* Transition: '<S53>:26' */
      /* Exit 'SDC_Opened': '<S53>:24' */
      rtDW.SDC_Opened = false;

      /*  ETAS hat 240 msec um IMC-Strom zu reduzieren, Danach �ffnen die AIRs Low-Side */
      /*  Bei einem AMS-Fehler wird nach 240 msec zuerst Lowside-Abgeschaltet und danach auf der */
      /*  Highside das Error-Flip-Flop gesetzt. */
      /*  So wird die sichere / f�r die AIR's schonende Abschaltung des TS sichergestellt */
      /*  240 msec aufgrund der Zykluszeit von 20 msec */
      rtDW.is_No_AMS_Error = IN_TS_Deactivated;
      rtDW.temporalCounter_i1 = 0U;

      /* Entry 'TS_Deactivated': '<S53>:13' */
      /*  TS ausgeschaltet */
      /*  AIRs deaktiviert, Precharge deaktiviert */
    } else {
      rtDW.SDC_Opened = true;
    }
    break;

   case IN_TS_Active:
    /* During 'TS_Active': '<S53>:22' */
    if ((!*LogicalOperator7) || rtDW.LogicalOperator1_p) {
      /* Transition: '<S53>:82' */
      /* Exit 'TS_Active': '<S53>:22' */
      rtDW.TS_Active = false;

      /*  Tractive System ist aktiv, Fahren m�glich */
      /*  Bei einem AMS-Fehler wird sicher abgeschalten */
      /*  Und erst nach dem abschalten das AMS-Flip-Flop */
      /*  gesetzt */
      rtDW.is_No_AMS_Error = IN_SDC_Opened;
      rtDW.temporalCounter_i1 = 0U;
    } else {
      rtDW.TS_Active = true;
    }
    break;

   default:
    /* During 'TS_Deactivated': '<S53>:13' */
    if ((*LogicalOperator7) && (rtDW.temporalCounter_i1 >= 50U)) {
      /* Transition: '<S53>:15' */
      rtDW.is_No_AMS_Error = IN_Precharge;
      rtDW.temporalCounter_i2 = 0U;

      /* Entry Internal 'Precharge': '<S53>:1' */
      /* Transition: '<S53>:7' */
      rtDW.is_Precharge = IN_Precharge_Init;

      /* Entry 'Precharge_Init': '<S53>:6' */
      /*  Auf AIR_N Warten */
    } else {
      if (rtDW.LogicalOperator1_p) {
        /* Transition: '<S53>:87' */
        /* Transition: '<S53>:39' */
        rtDW.is_No_AMS_Error = IN_NO_ACTIVE_CHILD;
        rtDW.is_c1_model = IN_AMS_Error_Active;
      }
    }
    break;
  }
}

/* Model step function */
void model_step(void)
{
  uint16_T minV;
  boolean_T rtb_LogicalOperator1;
  boolean_T LogicalOperator7;
  real_T Gain;
  real_T Gain1;
  int32_T i;

  /* S-Function (gpio_in): '<S3>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_m,
    &rtDW.SFunctionBuilder, &rtDW.SFunctionBuilder_DSTATE);

  /* S-Function (gpio_in): '<S1>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_p,
    &rtDW.SFunctionBuilder_e, &rtDW.SFunctionBuilder_DSTATE_b);

  /* Outputs for Enabled SubSystem: '<S21>/Voltage Estimation' incorporates:
   *  EnablePort: '<S54>/Enable'
   */
  /* UnitDelay: '<S21>/Unit Delay' */
  if (rtDW.UnitDelay_DSTATE) {
    if (!rtDW.VoltageEstimation_MODE) {
      /* InitializeConditions for DiscreteTransferFcn: '<S55>/Discrete Transfer Fcn' */
      rtDW.DiscreteTransferFcn_states = 0.0;
      rtDW.VoltageEstimation_MODE = true;
    }

    /* DiscreteTransferFcn: '<S55>/Discrete Transfer Fcn' */
    rtDW.DiscreteTransferFcn = 0.12482668095705254 *
      rtDW.DiscreteTransferFcn_states;
  } else {
    if (rtDW.VoltageEstimation_MODE) {
      rtDW.VoltageEstimation_MODE = false;
    }
  }

  /* End of UnitDelay: '<S21>/Unit Delay' */
  /* End of Outputs for SubSystem: '<S21>/Voltage Estimation' */

  /* S-Function (can_rx): '<S52>/S-Function Builder' */
  can_rx_Outputs_wrapper(&rtConstP.Constant_Value, &rtConstP.pooled13,
    &rtDW.SFunctionBuilder_o1, &rtDW.SFunctionBuilder_o2[0],
    &rtDW.SFunctionBuilder_DSTATE_bt);

  /* S-Function (scanpack): '<S20>/CAN Pack' */
  rtDW.CANPack.ID = 4U;
  rtDW.CANPack.Length = 8U;
  rtDW.CANPack.Extended = 0U;
  rtDW.CANPack.Remote = 0;
  rtDW.CANPack.Data[0] = 0;
  rtDW.CANPack.Data[1] = 0;
  rtDW.CANPack.Data[2] = 0;
  rtDW.CANPack.Data[3] = 0;
  rtDW.CANPack.Data[4] = 0;
  rtDW.CANPack.Data[5] = 0;
  rtDW.CANPack.Data[6] = 0;
  rtDW.CANPack.Data[7] = 0;

  {
    (void) memcpy((rtDW.CANPack.Data), &rtDW.SFunctionBuilder_o2[0],
                  8 * sizeof(uint8_T));
  }

  /* S-Function (scanunpack): '<S20>/CAN Unpack' */
  {
    /* S-Function (scanunpack): '<S20>/CAN Unpack' */
    if ((8 == rtDW.CANPack.Length) && (rtDW.CANPack.ID != INVALID_CAN_ID) ) {
      if ((4U == rtDW.CANPack.ID) && (0U == rtDW.CANPack.Extended) ) {
        {
          /* --------------- START Unpacking signal 0 ------------------
           *  startBit                = 1
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = SIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 1 ------------------
           *  startBit                = 0
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = SIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  int8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack.Data[0]) & (uint8_T)( (uint8_T) (1)<< 0))
                        >> 0)<<0);
                    }

                    unpackedValue = (int8_T) tempValue;
                  }

                  {
                    uint8_T tempValue = (uint8_T) unpackedValue;
                    uint8_T mask = (uint8_T)(1);
                    uint8_T bitValue = tempValue & (uint8_T)(mask << (1-1));
                    if (bitValue != 0U) {
                      {
                        tempValue = tempValue | (uint8_T)(mask << (8-0-1));
                      }

                      {
                        tempValue = tempValue | (uint8_T)(mask << (8-1-1));
                      }

                      {
                        tempValue = tempValue | (uint8_T)(mask << (8-2-1));
                      }

                      {
                        tempValue = tempValue | (uint8_T)(mask << (8-3-1));
                      }

                      {
                        tempValue = tempValue | (uint8_T)(mask << (8-4-1));
                      }

                      {
                        tempValue = tempValue | (uint8_T)(mask << (8-5-1));
                      }

                      {
                        tempValue = tempValue | (uint8_T)(mask << (8-6-1));
                      }

                      unpackedValue = (int8_T) tempValue;
                    }
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack_o2 = result;
              }
            }
          }
        }
      }
    }
  }

  /* S-Function (gpio_in): '<S23>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled8, &rtConstB.Subtract_e,
    &rtDW.SFunctionBuilder_b, &rtDW.SFunctionBuilder_DSTATE_i);

  /* Logic: '<Root>/Logical Operator7' */
  LogicalOperator7 = ((rtDW.CANUnpack_o2 != 0.0) && rtDW.SFunctionBuilder_b);

  /* S-Function (can_rx): '<S28>/S-Function Builder' */
  can_rx_Outputs_wrapper(&rtConstP.Constant_Value_p, &rtConstP.pooled13,
    &rtDW.SFunctionBuilder_o1_n, &rtDW.SFunctionBuilder_o2_m[0],
    &rtDW.SFunctionBuilder_DSTATE_k);

  /* S-Function (scanpack): '<S9>/CAN Pack2' */
  rtDW.CANPack2.ID = 1314U;
  rtDW.CANPack2.Length = 6U;
  rtDW.CANPack2.Extended = 0U;
  rtDW.CANPack2.Remote = 0;
  rtDW.CANPack2.Data[0] = 0;
  rtDW.CANPack2.Data[1] = 0;
  rtDW.CANPack2.Data[2] = 0;
  rtDW.CANPack2.Data[3] = 0;
  rtDW.CANPack2.Data[4] = 0;
  rtDW.CANPack2.Data[5] = 0;
  rtDW.CANPack2.Data[6] = 0;
  rtDW.CANPack2.Data[7] = 0;

  {
    (void) memcpy((rtDW.CANPack2.Data), &rtDW.SFunctionBuilder_o2_m[0],
                  6 * sizeof(uint8_T));
  }

  /* S-Function (scanunpack): '<S9>/CAN Unpack1' */
  {
    /* S-Function (scanunpack): '<S9>/CAN Unpack1' */
    if ((6 == rtDW.CANPack2.Length) && (rtDW.CANPack2.ID != INVALID_CAN_ID) ) {
      if ((1314U == rtDW.CANPack2.ID) && (0U == rtDW.CANPack2.Extended) ) {
        {
          /* --------------- START Unpacking signal 0 ------------------
           *  startBit                = 0
           *  length                  = 8
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 1 ------------------
           *  startBit                = 8
           *  length                  = 4
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 2 ------------------
           *  startBit                = 40
           *  length                  = 32
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = SIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  int32_T unpackedValue = 0;

                  {
                    uint32_T tempValue = (uint32_T) (0);

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        0)) >> 0)<<0);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        1)) >> 1)<<1);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        2)) >> 2)<<2);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        3)) >> 3)<<3);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        4)) >> 4)<<4);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        5)) >> 5)<<5);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        6)) >> 6)<<6);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        7)) >> 7)<<7);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        0)) >> 0)<<8);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        1)) >> 1)<<9);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        2)) >> 2)<<10);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        3)) >> 3)<<11);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        4)) >> 4)<<12);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        5)) >> 5)<<13);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        6)) >> 6)<<14);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        7)) >> 7)<<15);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        0)) >> 0)<<16);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        1)) >> 1)<<17);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        2)) >> 2)<<18);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        3)) >> 3)<<19);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        4)) >> 4)<<20);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        5)) >> 5)<<21);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        6)) >> 6)<<22);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        7)) >> 7)<<23);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        0)) >> 0)<<24);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        1)) >> 1)<<25);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        2)) >> 2)<<26);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        3)) >> 3)<<27);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        4)) >> 4)<<28);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        5)) >> 5)<<29);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        6)) >> 6)<<30);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack2.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        7)) >> 7)<<31);
                    }

                    unpackedValue = (int32_T) tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.IVT_Result_U1_mV = result;
              }
            }
          }

          /* --------------- START Unpacking signal 3 ------------------
           *  startBit                = 13
           *  length                  = 1
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 4 ------------------
           *  startBit                = 14
           *  length                  = 1
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 5 ------------------
           *  startBit                = 12
           *  length                  = 1
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 6 ------------------
           *  startBit                = 15
           *  length                  = 1
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */
        }
      }
    }
  }

  /* Gain: '<S9>/Gain' */
  Gain = 0.001 * rtDW.IVT_Result_U1_mV;

  /* S-Function (can_rx): '<S25>/S-Function Builder' */
  can_rx_Outputs_wrapper(&rtConstP.Constant_Value_p1, &rtConstP.pooled13,
    &rtDW.SFunctionBuilder_o1_o, &rtDW.SFunctionBuilder_o2_l[0],
    &rtDW.SFunctionBuilder_DSTATE_bl);

  /* S-Function (scanpack): '<S9>/CAN Pack1' */
  rtDW.CANPack1.ID = 1315U;
  rtDW.CANPack1.Length = 6U;
  rtDW.CANPack1.Extended = 0U;
  rtDW.CANPack1.Remote = 0;
  rtDW.CANPack1.Data[0] = 0;
  rtDW.CANPack1.Data[1] = 0;
  rtDW.CANPack1.Data[2] = 0;
  rtDW.CANPack1.Data[3] = 0;
  rtDW.CANPack1.Data[4] = 0;
  rtDW.CANPack1.Data[5] = 0;
  rtDW.CANPack1.Data[6] = 0;
  rtDW.CANPack1.Data[7] = 0;

  {
    (void) memcpy((rtDW.CANPack1.Data), &rtDW.SFunctionBuilder_o2_l[0],
                  6 * sizeof(uint8_T));
  }

  /* S-Function (scanunpack): '<S9>/CAN Unpack2' */
  {
    /* S-Function (scanunpack): '<S9>/CAN Unpack2' */
    if ((6 == rtDW.CANPack1.Length) && (rtDW.CANPack1.ID != INVALID_CAN_ID) ) {
      if ((1315U == rtDW.CANPack1.ID) && (0U == rtDW.CANPack1.Extended) ) {
        {
          /* --------------- START Unpacking signal 0 ------------------
           *  startBit                = 0
           *  length                  = 8
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 1 ------------------
           *  startBit                = 8
           *  length                  = 4
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 2 ------------------
           *  startBit                = 40
           *  length                  = 32
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = SIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  int32_T unpackedValue = 0;

                  {
                    uint32_T tempValue = (uint32_T) (0);

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        0)) >> 0)<<0);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        1)) >> 1)<<1);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        2)) >> 2)<<2);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        3)) >> 3)<<3);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        4)) >> 4)<<4);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        5)) >> 5)<<5);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        6)) >> 6)<<6);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[5]) & (uint32_T)( (uint32_T) (1)<<
                        7)) >> 7)<<7);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        0)) >> 0)<<8);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        1)) >> 1)<<9);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        2)) >> 2)<<10);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        3)) >> 3)<<11);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        4)) >> 4)<<12);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        5)) >> 5)<<13);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        6)) >> 6)<<14);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[4]) & (uint32_T)( (uint32_T) (1)<<
                        7)) >> 7)<<15);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        0)) >> 0)<<16);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        1)) >> 1)<<17);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        2)) >> 2)<<18);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        3)) >> 3)<<19);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        4)) >> 4)<<20);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        5)) >> 5)<<21);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        6)) >> 6)<<22);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[3]) & (uint32_T)( (uint32_T) (1)<<
                        7)) >> 7)<<23);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        0)) >> 0)<<24);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        1)) >> 1)<<25);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        2)) >> 2)<<26);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        3)) >> 3)<<27);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        4)) >> 4)<<28);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        5)) >> 5)<<29);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        6)) >> 6)<<30);
                    }

                    {
                      tempValue = tempValue | (uint32_T)((uint32_T)((uint32_T)
                        ((rtDW.CANPack1.Data[2]) & (uint32_T)( (uint32_T) (1)<<
                        7)) >> 7)<<31);
                    }

                    unpackedValue = (int32_T) tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.IVT_Result_U2_mV = result;
              }
            }
          }

          /* --------------- START Unpacking signal 3 ------------------
           *  startBit                = 13
           *  length                  = 1
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 4 ------------------
           *  startBit                = 14
           *  length                  = 1
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 5 ------------------
           *  startBit                = 12
           *  length                  = 1
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 6 ------------------
           *  startBit                = 15
           *  length                  = 1
           *  desiredSignalByteLayout = BIGENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */
        }
      }
    }
  }

  /* Gain: '<S9>/Gain1' */
  Gain1 = 0.001 * rtDW.IVT_Result_U2_mV;

  /* S-Function (BMoS_Input): '<S8>/BMoS_Input' */
  BMoS_Input_Outputs_wrapper( &rtDW.BMoS_Input_o1[0], &rtDW.BMoS_Input_o2[0],
    &rtDW.BMoS_Input_o3[0], &rtDW.BMoS_Input_DSTATE);

  /* S-Function (gpio_in): '<S7>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_a,
    &rtDW.SFunctionBuilder_j, &rtDW.SFunctionBuilder_DSTATE_h);

  /* Outputs for Enabled SubSystem: '<S30>/ON Delay' */

  /* Constant: '<S30>/Constant1' */
  ONDelay(true, rtDW.SFunctionBuilder_j, 0.99999999999999978,
          &rtDW.LogicalOperator1_n, &rtDW.ONDelay_o, 0.02);

  /* End of Outputs for SubSystem: '<S30>/ON Delay' */

  /* Outputs for Enabled SubSystem: '<S30>/OFF Delay' */
  OFFDelay(rtConstB.LogicalOperator2, rtDW.SFunctionBuilder_j,
           0.99999999999999978, &rtDW.LogicalOperator3_c, &rtDW.OFFDelay_n, 0.02);

  /* End of Outputs for SubSystem: '<S30>/OFF Delay' */

  /* Logic: '<S30>/Logical Operator1' */
  rtDW.LogicalOperator1 = (rtDW.LogicalOperator1_n || rtDW.LogicalOperator3_c);

  /* Outputs for Enabled SubSystem: '<Root>/Subsystem' incorporates:
   *  EnablePort: '<S22>/Enable'
   */
  if (rtDW.LogicalOperator1) {
    if (!rtDW.Subsystem_MODE) {
      rtDW.Subsystem_MODE = true;
    }

    /* MinMax: '<S22>/MinMax' */
    minV = rtDW.BMoS_Input_o1[0];
    for (i = 0; i < 143; i++) {
      if (!(minV <= rtDW.BMoS_Input_o1[i + 1])) {
        minV = rtDW.BMoS_Input_o1[i + 1];
      }
    }

    /* Logic: '<S22>/Logical Operator1' incorporates:
     *  MinMax: '<S22>/MinMax'
     *  RelationalOperator: '<S22>/Relational Operator1'
     *  RelationalOperator: '<S22>/Relational Operator4'
     */
    rtb_LogicalOperator1 = ((minV < 31000) && (minV > 28000));

    /* Outputs for Enabled SubSystem: '<S69>/ON Delay' */

    /* Constant: '<S69>/Constant1' */
    ONDelay(true, rtb_LogicalOperator1, 0.49999999999999978,
            &rtDW.LogicalOperator1_ox, &rtDW.ONDelay_l, 0.02);

    /* End of Outputs for SubSystem: '<S69>/ON Delay' */

    /* Outputs for Enabled SubSystem: '<S69>/OFF Delay' */
    OFFDelay(rtConstB.LogicalOperator2_f, rtb_LogicalOperator1,
             0.49999999999999978, &rtDW.LogicalOperator3_f, &rtDW.OFFDelay_e,
             0.02);

    /* End of Outputs for SubSystem: '<S69>/OFF Delay' */

    /* Logic: '<S22>/Logical Operator4' incorporates:
     *  Logic: '<S69>/Logical Operator1'
     */
    rtDW.BMoS_Error = (rtDW.LogicalOperator1_ox || rtDW.LogicalOperator3_f);
  } else {
    if (rtDW.Subsystem_MODE) {
      rtDW.Subsystem_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<Root>/Subsystem' */

  /* Logic: '<Root>/Logical Operator' incorporates:
   *  Logic: '<Root>/Logical Operator2'
   */
  rtb_LogicalOperator1 = (rtDW.BMoS_Error || (!rtDW.SFunctionBuilder_j));

  /* Outputs for Enabled SubSystem: '<S41>/ON Delay' */

  /* Constant: '<S41>/Constant1' */
  ONDelay(false, rtb_LogicalOperator1, 0.49999999999999978,
          &rtDW.LogicalOperator1_i, &rtDW.ONDelay_n, 0.02);

  /* End of Outputs for SubSystem: '<S41>/ON Delay' */

  /* Outputs for Enabled SubSystem: '<S41>/OFF Delay' */
  OFFDelay(rtConstB.LogicalOperator2_i, rtb_LogicalOperator1,
           0.49999999999999978, &rtDW.LogicalOperator3_a, &rtDW.OFFDelay_i, 0.02);

  /* End of Outputs for SubSystem: '<S41>/OFF Delay' */

  /* Logic: '<S41>/Logical Operator1' */
  rtDW.LogicalOperator1_p = (rtDW.LogicalOperator1_i || rtDW.LogicalOperator3_a);

  /* S-Function (gpio_in): '<S12>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_g,
    &rtDW.SFunctionBuilder_o, &rtDW.SFunctionBuilder_DSTATE_j);

  /* S-Function (gpio_in): '<S6>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_gz,
    &rtDW.SFunctionBuilder_h, &rtDW.SFunctionBuilder_DSTATE_l);

  /* S-Function (gpio_in): '<S18>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_pm,
    &rtDW.SFunctionBuilder_f, &rtDW.SFunctionBuilder_DSTATE_c);

  /* Chart: '<S21>/TS_Activation' */
  if (rtDW.temporalCounter_i1 < 127U) {
    rtDW.temporalCounter_i1++;
  }

  if (rtDW.temporalCounter_i2 < 255U) {
    rtDW.temporalCounter_i2++;
  }

  /* Gateway: Statemachine/TS_Activation */
  /* During: Statemachine/TS_Activation */
  if (rtDW.is_active_c1_model == 0U) {
    /* Entry: Statemachine/TS_Activation */
    rtDW.is_active_c1_model = 1U;

    /* Entry Internal: Statemachine/TS_Activation */
    /* Transition: '<S53>:37' */
    rtDW.is_c1_model = IN_No_AMS_Error;

    /* Entry 'No_AMS_Error': '<S53>:35' */
    /*  Kein Fehler aktiv, Aktivierung des TS m�glich */
    /* Entry Internal 'No_AMS_Error': '<S53>:35' */
    /* Transition: '<S53>:14' */
    rtDW.is_No_AMS_Error = IN_TS_Deactivated;
    rtDW.temporalCounter_i1 = 0U;

    /* Entry 'TS_Deactivated': '<S53>:13' */
    /*  TS ausgeschaltet */
    /*  AIRs deaktiviert, Precharge deaktiviert */
  } else {
    switch (rtDW.is_c1_model) {
     case IN_AMS_Error:
      /* During 'AMS_Error': '<S53>:36' */
      if ((!rtDW.SFunctionBuilder_h) && (!rtDW.SFunctionBuilder_o) &&
          (rtDW.temporalCounter_i1 >= 50U)) {
        /* Transition: '<S53>:38' */
        rtDW.is_c1_model = IN_No_AMS_Error;

        /* Entry 'No_AMS_Error': '<S53>:35' */
        /*  Kein Fehler aktiv, Aktivierung des TS m�glich */
        /* Entry Internal 'No_AMS_Error': '<S53>:35' */
        /* Transition: '<S53>:14' */
        rtDW.is_No_AMS_Error = IN_TS_Deactivated;
        rtDW.temporalCounter_i1 = 0U;

        /* Entry 'TS_Deactivated': '<S53>:13' */
        /*  TS ausgeschaltet */
        /*  AIRs deaktiviert, Precharge deaktiviert */
      } else {
        if (rtDW.LogicalOperator1_p) {
          /* Transition: '<S53>:93' */
          rtDW.is_c1_model = IN_AMS_Error_Active;
        }
      }
      break;

     case IN_AMS_Error_Active:
      /* During 'AMS_Error_Active': '<S53>:64' */
      if (!rtDW.LogicalOperator1_p) {
        /* Transition: '<S53>:65' */
        /* Exit 'AMS_Error_Active': '<S53>:64' */
        rtDW.AMS_Error = false;

        /*  AMS Fehler wird ausgel�st, */
        /*  SDC bleibt ge�ffnet */
        /*  Preset des AMS-Flip-Flops wird auf 'LOW' gezogen */
        rtDW.is_c1_model = IN_AMS_Error;
        rtDW.temporalCounter_i1 = 0U;

        /* Entry 'AMS_Error': '<S53>:36' */
        /*  Deaktivierung des TS wird gehalten */
        /*  Error Flip-Flop wird nicht mehr aktiv gesetzt */
      } else {
        rtDW.AMS_Error = true;
      }
      break;

     default:
      No_AMS_Error(&LogicalOperator7, &Gain, &Gain1);
      break;
    }
  }

  /* End of Chart: '<S21>/TS_Activation' */

  /* Logic: '<S21>/Logical Operator' */
  rtDW.AIR_N_EN = (rtDW.Precharge || rtDW.TS_Active || rtDW.SDC_Opened);

  /* S-Function (gpio_out): '<S2>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled8, &rtConstB.Subtract, &rtDW.AIR_N_EN,
    &rtDW.SFunctionBuilder_DSTATE_kf);

  /* Logic: '<S21>/Verriegelung_des_AIR_P' incorporates:
   *  Gain: '<S21>/Gain'
   *  Logic: '<S21>/Logical Operator1'
   *  RelationalOperator: '<S21>/Relational Operator'
   */
  rtDW.Verriegelung_des_AIR_P = ((Gain1 >= 0.9 * Gain) &&
    rtDW.SFunctionBuilder_e && rtDW.AIR_N_EN && (rtDW.TS_Active ||
    rtDW.Precharge_Finished || rtDW.SDC_Opened));

  /* S-Function (gpio_out): '<S4>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled8, &rtConstB.Subtract_k,
    &rtDW.Verriegelung_des_AIR_P, &rtDW.SFunctionBuilder_DSTATE_o);

  /* S-Function (gpio_out): '<S5>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_f,
    &rtDW.AMS_Error, &rtDW.SFunctionBuilder_DSTATE_e);

  /* DiscretePulseGenerator: '<Root>/Pulse Generator8' */
  rtDW.PulseGenerator8 = ((rtDW.clockTickCounter < 1) && (rtDW.clockTickCounter >=
    0));
  if (rtDW.clockTickCounter >= 4) {
    rtDW.clockTickCounter = 0;
  } else {
    rtDW.clockTickCounter++;
  }

  /* End of DiscretePulseGenerator: '<Root>/Pulse Generator8' */
  /* S-Function (scanpack): '<Root>/CAN Pack' */
  rtDW.CANPack_k.ID = 17U;
  rtDW.CANPack_k.Length = 2U;
  rtDW.CANPack_k.Extended = 0U;
  rtDW.CANPack_k.Remote = 0;
  rtDW.CANPack_k.Data[0] = 0;
  rtDW.CANPack_k.Data[1] = 0;
  rtDW.CANPack_k.Data[2] = 0;
  rtDW.CANPack_k.Data[3] = 0;
  rtDW.CANPack_k.Data[4] = 0;
  rtDW.CANPack_k.Data[5] = 0;
  rtDW.CANPack_k.Data[6] = 0;
  rtDW.CANPack_k.Data[7] = 0;

  {
    /* --------------- START Packing signal 0 ------------------
     *  startBit                = 1
     *  length                  = 1
     *  desiredSignalByteLayout = LITTLEENDIAN
     *  dataType                = UNSIGNED
     *  factor                  = 1.0
     *  offset                  = 0.0
     *  minimum                 = 0.0
     *  maximum                 = 0.0
     * -----------------------------------------------------------------------*/
    {
      {
        uint32_T packingValue = 0;

        {
          uint32_T result = (uint32_T) (rtDW.SFunctionBuilder_e);

          /* no scaling required */
          packingValue = result;
        }

        {
          {
            uint8_T packedValue;
            if (packingValue > (boolean_T)(1)) {
              packedValue = (uint8_T) 1;
            } else if (packingValue < (boolean_T)(0)) {
              packedValue = (uint8_T) 0;
            } else {
              packedValue = (uint8_T) (packingValue);
            }

            {
              {
                rtDW.CANPack_k.Data[0] = rtDW.CANPack_k.Data[0] | (uint8_T)
                  ((uint8_T)((uint8_T)(packedValue & (uint8_T)(((uint8_T)(1)) <<
                      0)) >> 0)<<1);
              }
            }
          }
        }
      }
    }

    /* --------------- START Packing signal 1 ------------------
     *  startBit                = 0
     *  length                  = 1
     *  desiredSignalByteLayout = LITTLEENDIAN
     *  dataType                = UNSIGNED
     *  factor                  = 1.0
     *  offset                  = 0.0
     *  minimum                 = 0.0
     *  maximum                 = 0.0
     * -----------------------------------------------------------------------*/
    {
      {
        uint32_T packingValue = 0;

        {
          uint32_T result = (uint32_T) (rtDW.SFunctionBuilder);

          /* no scaling required */
          packingValue = result;
        }

        {
          {
            uint8_T packedValue;
            if (packingValue > (boolean_T)(1)) {
              packedValue = (uint8_T) 1;
            } else if (packingValue < (boolean_T)(0)) {
              packedValue = (uint8_T) 0;
            } else {
              packedValue = (uint8_T) (packingValue);
            }

            {
              {
                rtDW.CANPack_k.Data[0] = rtDW.CANPack_k.Data[0] | (uint8_T)
                  ((uint8_T)((uint8_T)(packedValue & (uint8_T)(((uint8_T)(1)) <<
                      0)) >> 0)<<0);
              }
            }
          }
        }
      }
    }

    /* --------------- START Packing signal 2 ------------------
     *  startBit                = 3
     *  length                  = 1
     *  desiredSignalByteLayout = LITTLEENDIAN
     *  dataType                = UNSIGNED
     *  factor                  = 1.0
     *  offset                  = 0.0
     *  minimum                 = 0.0
     *  maximum                 = 0.0
     * -----------------------------------------------------------------------*/
    {
      {
        uint32_T packingValue = 0;

        {
          uint32_T result = (uint32_T) (rtDW.SFunctionBuilder_h);

          /* no scaling required */
          packingValue = result;
        }

        {
          {
            uint8_T packedValue;
            if (packingValue > (boolean_T)(1)) {
              packedValue = (uint8_T) 1;
            } else if (packingValue < (boolean_T)(0)) {
              packedValue = (uint8_T) 0;
            } else {
              packedValue = (uint8_T) (packingValue);
            }

            {
              {
                rtDW.CANPack_k.Data[0] = rtDW.CANPack_k.Data[0] | (uint8_T)
                  ((uint8_T)((uint8_T)(packedValue & (uint8_T)(((uint8_T)(1)) <<
                      0)) >> 0)<<3);
              }
            }
          }
        }
      }
    }

    /* --------------- START Packing signal 3 ------------------
     *  startBit                = 9
     *  length                  = 1
     *  desiredSignalByteLayout = LITTLEENDIAN
     *  dataType                = UNSIGNED
     *  factor                  = 1.0
     *  offset                  = 0.0
     *  minimum                 = 0.0
     *  maximum                 = 0.0
     * -----------------------------------------------------------------------*/
    {
      {
        uint32_T packingValue = 0;

        {
          uint32_T result = (uint32_T) (rtDW.LogicalOperator1);

          /* no scaling required */
          packingValue = result;
        }

        {
          {
            uint8_T packedValue;
            if (packingValue > (boolean_T)(1)) {
              packedValue = (uint8_T) 1;
            } else if (packingValue < (boolean_T)(0)) {
              packedValue = (uint8_T) 0;
            } else {
              packedValue = (uint8_T) (packingValue);
            }

            {
              {
                rtDW.CANPack_k.Data[1] = rtDW.CANPack_k.Data[1] | (uint8_T)
                  ((uint8_T)((uint8_T)(packedValue & (uint8_T)(((uint8_T)(1)) <<
                      0)) >> 0)<<1);
              }
            }
          }
        }
      }
    }

    /* --------------- START Packing signal 4 ------------------
     *  startBit                = 5
     *  length                  = 1
     *  desiredSignalByteLayout = LITTLEENDIAN
     *  dataType                = UNSIGNED
     *  factor                  = 1.0
     *  offset                  = 0.0
     *  minimum                 = 0.0
     *  maximum                 = 0.0
     * -----------------------------------------------------------------------*/
    {
      {
        uint32_T packingValue = 0;

        {
          uint32_T result = (uint32_T) (rtDW.LogicalOperator1_p);

          /* no scaling required */
          packingValue = result;
        }

        {
          {
            uint8_T packedValue;
            if (packingValue > (boolean_T)(1)) {
              packedValue = (uint8_T) 1;
            } else if (packingValue < (boolean_T)(0)) {
              packedValue = (uint8_T) 0;
            } else {
              packedValue = (uint8_T) (packingValue);
            }

            {
              {
                rtDW.CANPack_k.Data[0] = rtDW.CANPack_k.Data[0] | (uint8_T)
                  ((uint8_T)((uint8_T)(packedValue & (uint8_T)(((uint8_T)(1)) <<
                      0)) >> 0)<<5);
              }
            }
          }
        }
      }
    }

    /* --------------- START Packing signal 5 ------------------
     *  startBit                = 4
     *  length                  = 1
     *  desiredSignalByteLayout = LITTLEENDIAN
     *  dataType                = UNSIGNED
     *  factor                  = 1.0
     *  offset                  = 0.0
     *  minimum                 = 0.0
     *  maximum                 = 0.0
     * -----------------------------------------------------------------------*/
    {
      {
        uint32_T packingValue = 0;

        {
          uint32_T result = (uint32_T) (rtDW.SFunctionBuilder_o);

          /* no scaling required */
          packingValue = result;
        }

        {
          {
            uint8_T packedValue;
            if (packingValue > (boolean_T)(1)) {
              packedValue = (uint8_T) 1;
            } else if (packingValue < (boolean_T)(0)) {
              packedValue = (uint8_T) 0;
            } else {
              packedValue = (uint8_T) (packingValue);
            }

            {
              {
                rtDW.CANPack_k.Data[0] = rtDW.CANPack_k.Data[0] | (uint8_T)
                  ((uint8_T)((uint8_T)(packedValue & (uint8_T)(((uint8_T)(1)) <<
                      0)) >> 0)<<4);
              }
            }
          }
        }
      }
    }

    /* --------------- START Packing signal 6 ------------------
     *  startBit                = 2
     *  length                  = 1
     *  desiredSignalByteLayout = LITTLEENDIAN
     *  dataType                = UNSIGNED
     *  factor                  = 1.0
     *  offset                  = 0.0
     *  minimum                 = 0.0
     *  maximum                 = 0.0
     * -----------------------------------------------------------------------*/
  }

  /* S-Function (scanunpack): '<Root>/CAN Unpack' */
  {
    /* S-Function (scanunpack): '<Root>/CAN Unpack' */
    if ((2 == rtDW.CANPack_k.Length) && (rtDW.CANPack_k.ID != INVALID_CAN_ID) )
    {
      if ((17U == rtDW.CANPack_k.ID) && (0U == rtDW.CANPack_k.Extended) ) {
        (void) memcpy(&rtDW.MatrixConcatenate[0], rtDW.CANPack_k.Data,
                      2 * sizeof(uint8_T));
      }
    }
  }

  /* SignalConversion: '<Root>/ConcatBufferAtMatrix ConcatenateIn2' */
  for (i = 0; i < 6; i++) {
    rtDW.MatrixConcatenate[i + 2] = 0U;
  }

  /* End of SignalConversion: '<Root>/ConcatBufferAtMatrix ConcatenateIn2' */

  /* S-Function (can_tx): '<S10>/can_tx' */
  can_tx_Outputs_wrapper(&rtDW.PulseGenerator8, &rtConstP.Constant_Value_m,
    &rtConstP.Constant1_Value_l, &rtDW.MatrixConcatenate[0], &rtConstP.pooled13,
    &rtDW.can_tx_DSTATE);

  /* Logic: '<Root>/Logical Operator1' */
  rtDW.LogicalOperator1_o = !rtDW.LogicalOperator1_p;

  /* S-Function (gpio_out): '<S13>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_l,
    &rtDW.LogicalOperator1_o, &rtDW.SFunctionBuilder_DSTATE_h5);

  /* S-Function (gpio_out): '<S19>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_o,
    &rtDW.Precharge_Relay_EN, &rtDW.SFunctionBuilder_DSTATE_i0);

  /* Logic: '<Root>/Logical Operator3' */
  rtDW.LogicalOperator3 = !(rtDW.CANUnpack_o2 != 0.0);

  /* S-Function (gpio_out): '<S24>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_pf,
    &rtDW.LogicalOperator3, &rtDW.SFunctionBuilder_DSTATE_m);

  /* S-Function (can_rx): '<S27>/S-Function Builder' */
  can_rx_Outputs_wrapper(&rtConstP.Constant_Value_c, &rtConstP.pooled13,
    &rtDW.SFunctionBuilder_o1_f, &rtDW.SFunctionBuilder_o2_m2[0],
    &rtDW.SFunctionBuilder_DSTATE_he);

  /* DiscretePulseGenerator: '<Root>/Pulse Generator6' */
  LogicalOperator7 = ((rtDW.clockTickCounter_g < 10) && (rtDW.clockTickCounter_g
    >= 0));
  if (rtDW.clockTickCounter_g >= 49) {
    rtDW.clockTickCounter_g = 0;
  } else {
    rtDW.clockTickCounter_g++;
  }

  /* End of DiscretePulseGenerator: '<Root>/Pulse Generator6' */

  /* Logic: '<Root>/Logical Operator5' */
  rtDW.LogicalOperator5 = !LogicalOperator7;

  /* S-Function (gpio_out): '<S15>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_ai,
    &rtDW.LogicalOperator5, &rtDW.SFunctionBuilder_DSTATE_a);

  /* S-Function "gpio_in_wrapper" Block: <S3>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_m,
    &rtDW.SFunctionBuilder, &rtDW.SFunctionBuilder_DSTATE);

  /* S-Function "gpio_in_wrapper" Block: <S1>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_p,
    &rtDW.SFunctionBuilder_e, &rtDW.SFunctionBuilder_DSTATE_b);

  /* Update for UnitDelay: '<S21>/Unit Delay' */
  rtDW.UnitDelay_DSTATE = rtDW.Precharge_Relay_EN;

  /* Update for Enabled SubSystem: '<S21>/Voltage Estimation' incorporates:
   *  Update for EnablePort: '<S54>/Enable'
   */
  if (rtDW.VoltageEstimation_MODE) {
    /* Update for DiscreteTransferFcn: '<S55>/Discrete Transfer Fcn' */
    rtDW.DiscreteTransferFcn_states = Gain - -0.87517331904294748 *
      rtDW.DiscreteTransferFcn_states;
  }

  /* End of Update for SubSystem: '<S21>/Voltage Estimation' */

  /* S-Function "can_rx_wrapper" Block: <S52>/S-Function Builder */
  can_rx_Update_wrapper(&rtConstP.Constant_Value, &rtConstP.pooled13,
                        &rtDW.SFunctionBuilder_o1, &rtDW.SFunctionBuilder_o2[0],
                        &rtDW.SFunctionBuilder_DSTATE_bt);

  /* S-Function "gpio_in_wrapper" Block: <S23>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled8, &rtConstB.Subtract_e,
    &rtDW.SFunctionBuilder_b, &rtDW.SFunctionBuilder_DSTATE_i);

  /* S-Function "can_rx_wrapper" Block: <S28>/S-Function Builder */
  can_rx_Update_wrapper(&rtConstP.Constant_Value_p, &rtConstP.pooled13,
                        &rtDW.SFunctionBuilder_o1_n,
                        &rtDW.SFunctionBuilder_o2_m[0],
                        &rtDW.SFunctionBuilder_DSTATE_k);

  /* S-Function "can_rx_wrapper" Block: <S25>/S-Function Builder */
  can_rx_Update_wrapper(&rtConstP.Constant_Value_p1, &rtConstP.pooled13,
                        &rtDW.SFunctionBuilder_o1_o,
                        &rtDW.SFunctionBuilder_o2_l[0],
                        &rtDW.SFunctionBuilder_DSTATE_bl);

  /* S-Function "BMoS_Input_wrapper" Block: <S8>/BMoS_Input */
  BMoS_Input_Update_wrapper( &rtDW.BMoS_Input_o1[0], &rtDW.BMoS_Input_o2[0],
    &rtDW.BMoS_Input_o3[0], &rtDW.BMoS_Input_DSTATE);

  /* S-Function "gpio_in_wrapper" Block: <S7>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_a,
    &rtDW.SFunctionBuilder_j, &rtDW.SFunctionBuilder_DSTATE_h);

  /* S-Function "gpio_in_wrapper" Block: <S12>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_g,
    &rtDW.SFunctionBuilder_o, &rtDW.SFunctionBuilder_DSTATE_j);

  /* S-Function "gpio_in_wrapper" Block: <S6>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_gz,
    &rtDW.SFunctionBuilder_h, &rtDW.SFunctionBuilder_DSTATE_l);

  /* S-Function "gpio_in_wrapper" Block: <S18>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_pm,
    &rtDW.SFunctionBuilder_f, &rtDW.SFunctionBuilder_DSTATE_c);

  /* S-Function "gpio_out_wrapper" Block: <S2>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled8, &rtConstB.Subtract, &rtDW.AIR_N_EN,
    &rtDW.SFunctionBuilder_DSTATE_kf);

  /* S-Function "gpio_out_wrapper" Block: <S4>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled8, &rtConstB.Subtract_k,
    &rtDW.Verriegelung_des_AIR_P, &rtDW.SFunctionBuilder_DSTATE_o);

  /* S-Function "gpio_out_wrapper" Block: <S5>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_f,
    &rtDW.AMS_Error, &rtDW.SFunctionBuilder_DSTATE_e);

  /* S-Function "can_tx_wrapper" Block: <S10>/can_tx */
  can_tx_Update_wrapper(&rtDW.PulseGenerator8, &rtConstP.Constant_Value_m,
                        &rtConstP.Constant1_Value_l, &rtDW.MatrixConcatenate[0],
                        &rtConstP.pooled13, &rtDW.can_tx_DSTATE);

  /* S-Function "gpio_out_wrapper" Block: <S13>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_l,
    &rtDW.LogicalOperator1_o, &rtDW.SFunctionBuilder_DSTATE_h5);

  /* S-Function "gpio_out_wrapper" Block: <S19>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_o,
    &rtDW.Precharge_Relay_EN, &rtDW.SFunctionBuilder_DSTATE_i0);

  /* S-Function "gpio_out_wrapper" Block: <S24>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_pf,
    &rtDW.LogicalOperator3, &rtDW.SFunctionBuilder_DSTATE_m);

  /* S-Function "can_rx_wrapper" Block: <S27>/S-Function Builder */
  can_rx_Update_wrapper(&rtConstP.Constant_Value_c, &rtConstP.pooled13,
                        &rtDW.SFunctionBuilder_o1_f,
                        &rtDW.SFunctionBuilder_o2_m2[0],
                        &rtDW.SFunctionBuilder_DSTATE_he);

  /* S-Function "gpio_out_wrapper" Block: <S15>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled9, &rtConstB.Subtract_ai,
    &rtDW.LogicalOperator5, &rtDW.SFunctionBuilder_DSTATE_a);
}

/* Model initialize function */
void model_initialize(void)
{
  /*-----------S-Function Block: <S20>/CAN Unpack -----------------*/

  /*-----------S-Function Block: <S9>/CAN Unpack1 -----------------*/

  /*-----------S-Function Block: <S9>/CAN Unpack2 -----------------*/

  /*-----------S-Function Block: <Root>/CAN Unpack -----------------*/

  /* S-Function Block: <S3>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <S1>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_b = initVector[0];
      }
    }
  }

  /* S-Function Block: <S52>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_bt = initVector[0];
      }
    }
  }

  /* S-Function Block: <S23>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_i = initVector[0];
      }
    }
  }

  /* S-Function Block: <S28>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_k = initVector[0];
      }
    }
  }

  /* S-Function Block: <S25>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_bl = initVector[0];
      }
    }
  }

  /* S-Function Block: <S8>/BMoS_Input */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.BMoS_Input_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <S7>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_h = initVector[0];
      }
    }
  }

  /* S-Function Block: <S12>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_j = initVector[0];
      }
    }
  }

  /* S-Function Block: <S6>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_l = initVector[0];
      }
    }
  }

  /* S-Function Block: <S18>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_c = initVector[0];
      }
    }
  }

  /* S-Function Block: <S2>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_kf = initVector[0];
      }
    }
  }

  /* S-Function Block: <S4>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_o = initVector[0];
      }
    }
  }

  /* S-Function Block: <S5>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_e = initVector[0];
      }
    }
  }

  /* S-Function Block: <S10>/can_tx */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.can_tx_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <S13>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_h5 = initVector[0];
      }
    }
  }

  /* S-Function Block: <S19>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_i0 = initVector[0];
      }
    }
  }

  /* S-Function Block: <S24>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_m = initVector[0];
      }
    }
  }

  /* S-Function Block: <S27>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_he = initVector[0];
      }
    }
  }

  /* S-Function Block: <S15>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_a = initVector[0];
      }
    }
  }

  /* SystemInitialize for Enabled SubSystem: '<S30>/ON Delay' */
  ONDelay_Init(&rtDW.ONDelay_o, false);

  /* End of SystemInitialize for SubSystem: '<S30>/ON Delay' */

  /* SystemInitialize for Enabled SubSystem: '<S30>/OFF Delay' */
  OFFDelay_Init(&rtDW.OFFDelay_n, false);

  /* End of SystemInitialize for SubSystem: '<S30>/OFF Delay' */

  /* SystemInitialize for Enabled SubSystem: '<Root>/Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S69>/ON Delay' */
  ONDelay_Init(&rtDW.ONDelay_l, false);

  /* End of SystemInitialize for SubSystem: '<S69>/ON Delay' */

  /* SystemInitialize for Enabled SubSystem: '<S69>/OFF Delay' */
  OFFDelay_Init(&rtDW.OFFDelay_e, false);

  /* End of SystemInitialize for SubSystem: '<S69>/OFF Delay' */

  /* End of SystemInitialize for SubSystem: '<Root>/Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S41>/ON Delay' */
  ONDelay_Init(&rtDW.ONDelay_n, false);

  /* End of SystemInitialize for SubSystem: '<S41>/ON Delay' */

  /* SystemInitialize for Enabled SubSystem: '<S41>/OFF Delay' */
  OFFDelay_Init(&rtDW.OFFDelay_i, false);

  /* End of SystemInitialize for SubSystem: '<S41>/OFF Delay' */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
