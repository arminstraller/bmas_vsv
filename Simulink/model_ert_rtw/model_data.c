/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: model_data.c
 *
 * Code generated for Simulink model 'model'.
 *
 * Model version                  : 1.289
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Fri Jun 22 20:22:57 2018
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "model.h"

/* Invariant block signals (auto storage) */
const ConstB rtConstB = {
  2,                                   /* '<S2>/Subtract' */
  14,                                  /* '<S3>/Subtract' */
  8,                                   /* '<S1>/Subtract' */
  1,                                   /* '<S23>/Subtract' */
  12,                                  /* '<S7>/Subtract' */
  10,                                  /* '<S12>/Subtract' */
  11,                                  /* '<S6>/Subtract' */
  14,                                  /* '<S18>/Subtract' */
  0,                                   /* '<S4>/Subtract' */
  12,                                  /* '<S5>/Subtract' */
  0,                                   /* '<S13>/Subtract' */
  1,                                   /* '<S19>/Subtract' */
  5,                                   /* '<S24>/Subtract' */
  1,                                   /* '<S15>/Subtract' */
  0,                                   /* '<S30>/Logical Operator2' */
  1,                                   /* '<S41>/Logical Operator2' */
  0                                    /* '<S69>/Logical Operator2' */
};

/* Constant parameters (auto storage) */
const ConstP rtConstP = {
  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S1>/Constant2'
   *   '<S2>/Constant'
   *   '<S2>/Constant2'
   *   '<S3>/Constant2'
   *   '<S4>/Constant'
   *   '<S4>/Constant1'
   *   '<S4>/Constant2'
   *   '<S5>/Constant2'
   *   '<S6>/Constant2'
   *   '<S7>/Constant2'
   *   '<S12>/Constant2'
   *   '<S13>/Constant1'
   *   '<S13>/Constant2'
   *   '<S15>/Constant2'
   *   '<S18>/Constant2'
   *   '<S19>/Constant2'
   *   '<S23>/Constant'
   *   '<S23>/Constant2'
   *   '<S24>/Constant2'
   */
  1,

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S1>/Constant'
   *   '<S2>/Constant1'
   *   '<S3>/Constant'
   *   '<S5>/Constant'
   *   '<S6>/Constant'
   *   '<S12>/Constant'
   *   '<S13>/Constant'
   *   '<S15>/Constant'
   *   '<S24>/Constant'
   */
  3,

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S7>/Constant'
   *   '<S15>/Constant1'
   *   '<S18>/Constant'
   *   '<S19>/Constant'
   *   '<S19>/Constant1'
   *   '<S23>/Constant1'
   */
  2,

  /* Computed Parameter: Constant_Value
   * Referenced by: '<S52>/Constant'
   */
  4U,

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S10>/Constant2'
   *   '<S25>/Constant1'
   *   '<S27>/Constant1'
   *   '<S28>/Constant1'
   *   '<S52>/Constant1'
   */
  1U,

  /* Computed Parameter: Constant_Value_p
   * Referenced by: '<S28>/Constant'
   */
  1314U,

  /* Computed Parameter: Constant_Value_p1
   * Referenced by: '<S25>/Constant'
   */
  1315U,

  /* Computed Parameter: Constant_Value_m
   * Referenced by: '<S10>/Constant'
   */
  17U,

  /* Computed Parameter: Constant1_Value_l
   * Referenced by: '<S10>/Constant1'
   */
  2U,

  /* Computed Parameter: Constant_Value_c
   * Referenced by: '<S27>/Constant'
   */
  1320U
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
